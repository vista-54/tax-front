import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProgressBarComponent} from './components/progress-bar/progress-bar.component';
import {AutocompleteComponentComponent} from './components/autocomplete-component/autocomplete-component.component';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatStepperModule} from "@angular/material/stepper";


@NgModule({
    declarations: [
        ProgressBarComponent,
        AutocompleteComponentComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        MatInputModule,
      MatStepperModule
    ],
    entryComponents: [
        ProgressBarComponent,
        AutocompleteComponentComponent
    ],
    providers: [
        ProgressBarComponent,
        AutocompleteComponentComponent
    ],
    exports: [
        ProgressBarComponent,
        AutocompleteComponentComponent
    ]
})
export class SharedModule {
}
