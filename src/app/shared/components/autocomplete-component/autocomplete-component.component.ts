import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {} from 'googlemaps';

declare const google;

@Component({
  selector: 'app-autocomplete-component',
  templateUrl: './autocomplete-component.component.html',
  styleUrls: ['./autocomplete-component.component.css']
})
export class AutocompleteComponentComponent implements OnInit, AfterViewInit {
  @Input() adressType: string;
  @Input() placeholder: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext') addresstext: any;
  @Input() autocompleteInput: string;
  queryWait: boolean;

  constructor() {
  }


  ngOnInit() {

  }
  ngAfterViewInit() {
    this.getPlaceAutocomplete();
    console.log(this.placeholder);
  }

  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
      {
        // componentRestrictions: {},
        types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
      });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);
    });
  }

  invokeEvent(place: Object) {
    this.setAddress.emit(place);
  }

  updateModel() {
    this.setAddress.emit(this.autocompleteInput);
  }


}
