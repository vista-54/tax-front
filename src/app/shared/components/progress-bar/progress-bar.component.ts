import {Component, OnInit} from '@angular/core';
import {ProgressService} from '../../services/progress.service';

declare const document;

declare interface StepI {
    completed: boolean;
}

@Component({
    selector: 'app-progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {

    value: any;
    steps: StepI[];
    selectedIndex: number;

    constructor(private progress: ProgressService) {
        this.steps = [];
        this.selectedIndex = 0;
    }

    ngOnInit(): void {
        this.progress.currentProgress.subscribe((res) => {
            this.selectedIndex = res - 1;
            for (var i = 1; i < 29; i++) {
                if (i < res) {
                    this.steps.push({
                        completed: true,
                    });
                } else {
                    this.steps.push({
                        completed: false
                    });
                }
            }
        });
    }

}
