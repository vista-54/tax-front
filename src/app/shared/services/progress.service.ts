import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressService {
    private progressSource = new BehaviorSubject(localStorage['progress']);

    currentProgress = this.progressSource.asObservable();

    constructor() {
    }

    changeProgress(newProgress) {
        this.progressSource.next(newProgress);
        localStorage['progress'] = newProgress;
    }
}
