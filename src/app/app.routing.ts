import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
// import { P500Component } from './views/error/500.component';
import { LoginComponent } from './users/login/login.component';
import { RegisterComponent } from './users/register/register.component';
import { ListingComponent } from './users/listing/listing.component';
import { AuthGuard } from './guards/auth-guard.service';
import {ChangePasswordComponent} from './users/change-password/change-password.component';
import {ForgotPasswordComponent} from './users/forgot-password/forgot-password.component';
// import { DashboardComponent } from './dashboard/dashboard.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',

  },

  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent,
    data: {
      title: 'Forgot Password Page'
    }
  },
  {
    path: 'change-password/:token/:email',
    component: ChangePasswordComponent,
    data: {
      title: 'Change Password'
    }
  },
  {
    path: 'users',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Users Information'
    },
    loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
  },
  {
    path: 'llc-information',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'LLC Information'
    },
    loadChildren: () => import('./llc-information/llc-information.module').then(m => m.LlcInformationModule)
  },
  {
    path: 'operating-agreement',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Operating Agreement'
    },
    loadChildren: () => import('./operating-agreement/operating-agreement.module').then(m => m.OperatingAgreementModule)
  },
  {
    path: 'aitax-online',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Ai Tax Online'
    },
    loadChildren: () => import('./aitax-online/aitax-online.module').then(m => m.AitaxOnlineModule)
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        canActivate: [AuthGuard],
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      }
    ]
  },
  // { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
