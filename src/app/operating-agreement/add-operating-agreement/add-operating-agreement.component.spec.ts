import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddOperatingAgreementComponent } from './add-operating-agreement.component';

describe('AddOperatingAgreementComponent', () => {
  let component: AddOperatingAgreementComponent;
  let fixture: ComponentFixture<AddOperatingAgreementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddOperatingAgreementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddOperatingAgreementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
