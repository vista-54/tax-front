import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Config } from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class OperatingAgreementService {

  constructor(private http: HttpClient,private config: Config) { }

  
  save(formData: object): Observable<any> {
		let url = this.config.getConfig('apiUrl') + 'operating-agreement';
		return this.http.post(url, formData);
  }

  get(id?: any): Observable<any> {
		let url = this.config.getConfig('apiUrl') + 'operating-agreement/'+ id;
		return this.http.get(url);
  }
  
  filePreview(id?: any): Observable<any> {
		let url = this.config.getConfig('apiUrl') + 'operating-agreement-doc/'+ id;
		return this.http.get(url);
  }
  
}
