import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddOperatingAgreementComponent } from './add-operating-agreement/add-operating-agreement.component';
import { OperatingAgreementRoutingModule } from './operating-agreement.routing.module';
import { ToastrService } from 'ngx-toastr';


@NgModule({
  declarations: [AddOperatingAgreementComponent],
  imports: [
    CommonModule,
    OperatingAgreementRoutingModule
  ],
	providers: [
		ToastrService,

	]

})
export class OperatingAgreementModule { }
