import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddOperatingAgreementComponent } from './add-operating-agreement/add-operating-agreement.component';


const routes: Routes = [
  {
    path: '',
    component: AddOperatingAgreementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OperatingAgreementRoutingModule { }