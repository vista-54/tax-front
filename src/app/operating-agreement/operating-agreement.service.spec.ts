import { TestBed } from '@angular/core/testing';

import { OperatingAgreementService } from './operating-agreement.service';

describe('OperatingAgreementService', () => {
  let service: OperatingAgreementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OperatingAgreementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
