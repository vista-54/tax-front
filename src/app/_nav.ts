import {INavData} from '@coreui/angular';

export const navItemsUser: INavData[] = [
    {
        name: 'Annual Tax Interview',
        url: '/aitax-online',
        icon: 'icon-pencil'
    }

];
export const navItemsAdmin: INavData[] = [
    {
        name: 'Users',
        url: '/users',
        icon: 'icon-person',
    }

];
