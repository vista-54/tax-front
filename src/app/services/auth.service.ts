import {Injectable} from '@angular/core';
import * as moment from 'moment';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {User} from '../users/user';
import {Config} from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private config: Config) {

  }

  login(email: string, password: string) {
    let url = this.config.getConfig('apiUrl') + 'login';
    return this.http.post<User>(url, {email, password});
  }

  logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('user_info');
  }

  public isLoggedIn() {
    return (moment().isBefore(this.getExpiration()) || localStorage.getItem('access_token'));
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  currentUser() {
    const current_user = localStorage.getItem('current_user');
    return JSON.parse(current_user);
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  forgotPass(_email) {
    let url = this.config.getConfig('apiUrl') + 'password/email';
    return this.http.post(url, {email: _email});
  }

  /*request for  change password functionality*/

  changePass(form, token, email) {
    let url = this.config.getConfig('apiUrl') + 'reset/password';
    return this.http.post(url, {
      token: token,
      email: email,
      password: form.password.value,
      password_confirmation: form.passwordConfirm.value
    });
  }
}
