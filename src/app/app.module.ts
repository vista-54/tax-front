import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

import {AppComponent} from './app.component';

// Import containers
import {DefaultLayoutComponent} from './containers';

import {P404Component} from './views/error/404.component';
// import { P500Component } from './views/error/500.component';
// import { ForgotPasswordComponent } from './users/login/login.component';
import {RegisterComponent} from './views/register/register.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


const APP_CONTAINERS = [
    DefaultLayoutComponent
];

import {
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import {AppRoutingModule} from './app.routing';

// Import 3rd party components
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {ChartsModule} from 'ng2-charts';
import {UsersModule} from './users/users.module';
// import { DashboardComponent } from './dashboard/dashboard.component';
import {ToastrModule} from 'ngx-toastr';
import {JwtInterceptor} from './helpers/jwt.interceptor';
import {JasperoConfirmationsModule} from '@jaspero/ng-confirmations';
import {Config} from './config/config';
import {AuthGuard} from './guards/auth-guard.service';
import {ProgressBarComponent} from './shared/components/progress-bar/progress-bar.component';
import {SharedModule} from './shared/shared.module';
import {NgxCaptchaModule} from 'ngx-captcha';
// import { JwtModule } from "@auth0/angular-jwt";

// export function tokenGetter() {
//   return localStorage.getItem("access_token");
// }
@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        AppAsideModule,
        AppBreadcrumbModule.forRoot(),
        AppFooterModule,
        AppHeaderModule,
        AppSidebarModule,
        PerfectScrollbarModule,
        BsDropdownModule.forRoot(),
        TabsModule.forRoot(),
        ChartsModule,
        UsersModule,
        SharedModule,
        FormsModule,
        NgxCaptchaModule,
        ToastrModule.forRoot({
            timeOut: 5000,
            // positionClass: 'toast-bottom-right'
        }),
        HttpClientModule,
        JasperoConfirmationsModule.forRoot(),
        // JwtModule.forRoot({
        //   config: {
        //     tokenGetter: tokenGetter,
        //     whitelistedDomains: ["http://localhost"],
        //     // blacklistedRoutes: ["example.com/examplebadroute/"]
        //   }
        // }),
        NgbModule
    ],
    exports: [
        SharedModule
    ],
    declarations: [
        AppComponent,
        ...APP_CONTAINERS,
        RegisterComponent,
        P404Component,
        // DashboardComponent
    ],
    providers: [{
        provide: LocationStrategy,
        useClass: HashLocationStrategy
    }, {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        Config,
        AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
