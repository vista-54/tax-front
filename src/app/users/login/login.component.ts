import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {navItemsAdmin, navItemsUser} from '../../_nav';
import {ToastrService} from 'ngx-toastr';


declare const document;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  modalRef: NgbModalRef;


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private modalService: NgbModal,
              private toastrService: ToastrService,
              private _authService: AuthService) {


    // redirect to home if already logged in
    // console.log(this._authService.isLoggedIn);
    // if (this._authService.isLoggedIn()) {
    //   this.router.navigate(['']);
    // }
  }


  ngOnInit() {
    this.route.queryParams.subscribe(
      (queryParam: any) => {
        let verification = queryParam['verification'];
        console.log(verification);
        if (verification == 'verified') {
          setTimeout(() => {
            this.toastrService.success('Thank you for verifying your account. Please login to continue');
          }, 1000);
        }
      }
    );
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });


    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this._authService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.setSession(data);
          let role, user;
          if (data['result']['data']['user']['role']) {
            role = data['result']['data']['user']['role'];
            user = data['result']['data']['user'];
            console.log(user);
            localStorage.setItem('didHasReportToUnlock',data['result']['data']['report']);
            localStorage.setItem('role', JSON.stringify(role));
            localStorage.setItem('user', JSON.stringify(user));
          }
          if (role == 'ADMIN') {
            this.router.navigate(['users']);
          } else {
            this.router.navigate(['aitax-online/']);
          }
          console.log(data['result']['data']['user']['role']);

        },
        error => {
          console.log(error);
          if (error.status == 401)
            this.error = 'Invalid UserName/Password.';
          this.loading = false;
        });
  }

  private setSession(authResult) {
    const expiresAt = moment().add(authResult.expiresIn, 'second');
// ;
    localStorage.setItem('access_token', authResult.result.data.token);
    localStorage.setItem('user_id', authResult.result.data.user.id);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    localStorage.setItem('session_id', authResult.result.data.session_id);
  }

  signup() {
    this.router.navigate(['/register']);
  }

  forgotPass() {
    this.router.navigate(['/forgot-password']);
  };

}
