import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ConfirmationService, ResolveEmit} from '@jaspero/ng-confirmations';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {

  userRows = [];
  params: any = {};
  userPage: any = {
    //The number of elements in the page
    size: 0,
    //The total number of elements
    totalElements: 0,
    //The total number of pages
    totalPages: 0,
    //The current page number
    pageNumber: 0
  };
  sortEmail: string = 'ASC';
  sortName: string = 'ASC';
  sortSubmited: string = 'ASC';

  constructor(private userService: UserService,
              private toastrService: ToastrService,
              private router: Router,
              private _authService: AuthService,
              private _confirmation: ConfirmationService) {
    this.userPage.pageNumber = 0;
    this.userPage.size = 10;
  }

  ngOnInit() {
    this.nextPage({offset: 0});
  }

  pageLimit($num) {
    // console.log('pagelimitchange', $num);
    this.userPage.size = Number($num);
    this.nextPage({offset: 0});
    // if (this.cases) this.page.size = Number(num);
  }

  nextPage(pageInfo: any): void {
    console.log(pageInfo);
    this.userPage.pageNumber = pageInfo.offset;
    const pageNumber = this.userPage.pageNumber + 1;
    this.params = {
      per_page: +this.userPage.size,
      page: pageInfo.offset + 1,
      pagination: +1,
    };
    this.userService
      .getAllUsers(this.params)
      .subscribe(
        (comingData: any) => {
          console.log(this.params);
          console.log(comingData);
          // this.loadSpin = false;
          if (comingData['status'] == true || comingData['status'] == 200) {
            this.userRows = comingData.result ? comingData.result.data : [];
            this.userPage.totalRows = comingData.result.total;
          }
        },
        (err) => {
          if (err.status == 406) {
            let message = err.error.message.join(' ');
            console.log(message);
            this.toastrService.error(message);
          } else if (err.status == 401) {
            this._authService.logout();
            this.router.navigate(['/login']);
          }
        },
      );
  }

  sort(field: number) {
    let params: any;
    if (field === 1) {
      if (this.sortEmail === 'ASC') {
        this.sortEmail = 'DESC';
      } else {
        this.sortEmail = 'ASC';
      }
      params = {
        per_page: +this.userPage.size,
        page: this.userPage.pageNumber + 1,
        pagination: +1,
        sort: this.sortEmail,
        sort_field: 'email'
      };
    } else if (field === 0) {
      if (this.sortName === 'ASC') {
        this.sortName = 'DESC';
      } else {
        this.sortName = 'ASC';
      }
      params = {
        per_page: +this.userPage.size,
        page: this.userPage.pageNumber + 1,
        pagination: +1,
        sort: this.sortName,
        sort_field: 'first_name'
      };
    } else if (field === 2) {
      if (this.sortSubmited === 'ASC') {
        this.sortSubmited = 'DESC';
      } else {
        this.sortSubmited = 'ASC';
      }
      params = {
        per_page: +this.userPage.size,
        page: this.userPage.pageNumber + 1,
        pagination: +1,
        sort: this.sortSubmited,
        sort_field: 'report'
      };
    }


    this.userService
      .getSortUsers(params)
      .subscribe(
        (comingData: any) => {
          console.log(this.params);
          console.log(comingData);
          // this.loadSpin = false;
          if (comingData['status'] == true || comingData['status'] == 200) {
            this.userRows = comingData.result ? comingData.result.data : [];
            this.userPage.totalRows = comingData.result.total;
          }
        },
        (err) => {
          if (err.status == 406) {
            let message = err.error.message.join(' ');
            console.log(message);
            this.toastrService.error(message);
          } else if (err.status == 401) {
            this._authService.logout();
            this.router.navigate(['/login']);
          }
        },
      );
  }

  changeStatus(id: number, status: number) {

    console.log(status);
    // const options: ConfirmSettings;
    this._confirmation
      .create('Status Change Confirmation?', 'Do you really want to change status?')
      // The confirmation returns an Observable Subject which will notify you about the outcome
      .subscribe((ans: ResolveEmit) => {
        // console.log(ans);
        // ;
        if (ans.resolved) {
          console.log('in resolve function');
          const intStatus = +status;
          this.userService.changeStatus(id, intStatus).subscribe(
            (res) => {
              this.nextPage({offset: 0});
              this.toastrService.success('Status changed successfully.', 'Success!');
            },
            (err) => {
              this.toastrService.error('Failed to chagne status. please try again.', 'Error!');
            }
          );
          // this.deleteSelected(id);
        }
      });
    // this._confirmation.create("Confirmation", "Are you sure, you want to change status ?").subscribe(res => {})
  }

  downloadAiTaxDoc(userFormContent, row) {
    let token = localStorage.getItem('access_token');
    const url = this.userService.downloadAiTaxDoc(token, row['id']);
    window.open(url, '_blank');
    // this.deleteSelected(id);
  }

  confirmDel(row) {
    console.log(row);
    const url = this.userService.deleteUser(row).subscribe(
      (res) => {
        console.log(res);
        this.toastrService.success(res['message'], 'Success!');
      },
      (err) => {
        this.toastrService.error(`Failed to delete user with id ${row.id}`, 'Error!');
      }
    );
  }
}
