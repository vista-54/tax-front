import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Config} from '../config/config';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private config: Config) {
  }

  getAllUsers(params?: any): Observable<any> {
    let url = this.config.getConfig('apiUrl') + 'users?pagination=' + params.pagination + '&page=' + params.page + '&per_page=' + params.per_page;
    return this.http.get(url);
  }
  getSortUsers(params?: any): Observable<any> {
    let url = this.config.getConfig('apiUrl') + 'users?pagination=' + params.pagination + '&page=' + params.page + '&per_page=' + params.per_page  + '&sort=' + params.sort  + '&sort_field=' + params.sort_field;
    return this.http.get(url);
  }

  deleteUser(params?: any): Observable<any> {
    let url = this.config.getConfig('apiUrl') + 'users/' + params.id;
    return this.http.delete(url);
  }

  gitSingle(id?: any): Observable<any> {
    let url = this.config.getConfig('apiUrl') + 'single?id=' + id;
    return this.http.get(url);
  }

  saveUser(formData: object): Observable<any> {
    let url = this.config.getConfig('apiUrl') + 'register';
    return this.http.post(url, formData);
  }

  deleteUaser(id: number): Observable<any> {
    let url = this.config.getConfig('apiUrl') + 'delete';
    return this.http.post(url, {'id': id});
  }

  changeStatus(id: number, status: number): Observable<any> {
    let url = this.config.getConfig('apiUrl') + 'user/change-status';
    return this.http.post(url, {'id': id, 'status': status});
  }

  downloadAiTaxDoc(token, id) {
    const url = this.config.getConfig('apiUrl') + 'generate-pdf/' + id + '?token=' + token;
    return url;
  }

  touchAllFields(form) {
    // touch all fields to show the error
    Object.keys(form.controls).forEach(field => {
      const control = form.get(field);
      control.markAsTouched({onlySelf: true});
    });
  }
}
