import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../user.service';
import {first} from 'rxjs/operators';
import {AuthService} from '../../services/auth.service';
import {ToastrService} from 'ngx-toastr';
import {phoneCodes} from '../../shared/constants/phone-codes';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string = '/dashboaord';
  error = '';
  success = '';
  siteKey = '6LdVGr4ZAAAAAENZn_RyFhym3G8rKZj13tLTvlEW';
  phoneCods = phoneCodes;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService,
              private userService: UserService,
              private _authService: AuthService) {
    // if (this._authService.isLoggedIn()) {
    //   this.router.navigate(['']);
    // }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: [''],
      country_code: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
      recaptcha: ['', Validators.required]
    });
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {


    // stop here if form is invalid
    if (this.registerForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      // this.registerForm.setValue()
      this.userService.saveUser(this.registerForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.success = 'Registration Successful.  You will receive a confirmation email in a few seconds to activate your account.';
            this.toastrService.success('Thank you for registering with Ai Tax, please verify your e-mail address to continue');

            // this.router.navigate([this.returnUrl]);
          },
          error => {
            console.log(error);
            if (error.status == 422) {
              this.error = error.error.errors;
              // console.log(errors);
              // errors.forEach((idx, element) => {
              //   let message = idx + ': ' + element;
              //   this.error = message;
              // });
            }
            if (error.status == 500) {
              this.error = error.error.message;
            }
            if (error.status == 406) {
              this.error = error.error.message;
            }
            this.success = '';
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.userService.touchAllFields(this.registerForm);
    }
  }


  login() {
    this.router.navigate(['/login']);

  }

  back() {
    this.router.navigate(['login']);
  }

}
