import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';

declare const document;

@Component({
  selector: 'app-login',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  forgotForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  modalRef: NgbModalRef;
  email:any;
  token:any;


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private toastrService: ToastrService,
              private _authService: AuthService) {
    this.route.paramMap.subscribe(params => {
      this.token = params.get("token");
      this.email = params.get("email");
    });

  }

  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      // email: ['', Validators.required],
      // token: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirm: ['', Validators.required],
    });
  }

  get form() {
    return this.forgotForm.controls;
  }


  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.forgotForm.invalid) {
      return;
    }

    this.loading = true;
    console.log(this.token, this.email);
    this._authService.changePass(this.form, this.token, this.email)
      .pipe(first())
      .subscribe(
        data => {
          this.toastrService.success('Your password is changed!');
          this.router.navigate(['login']);
        },
        error => {

          console.log(error);
          if (error.status == 406) {
            this.error = 'Invalid Password.';
            this.toastrService.error(error.error['message']);
          }

          this.loading = false;
        });
  }

}
