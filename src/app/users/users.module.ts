import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';

import {BrowserModule} from '@angular/platform-browser';
// Import the library
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {TimezonePickerModule} from 'ng2-timezone-selector';
import {ListingComponent} from './listing/listing.component';
import {UserRoutingModule} from './users.routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';

import {JasperoConfirmationsModule} from '@jaspero/ng-confirmations';
import {EditUserComponent} from './edit-user/edit-user.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {NgxMaskModule} from 'ngx-mask';
import {NgxCaptchaModule} from 'ngx-captcha';

@NgModule({
    declarations: [LoginComponent, RegisterComponent, ListingComponent, EditUserComponent, ChangePasswordComponent, ForgotPasswordComponent],
    imports: [
        NgxDatatableModule,
        CommonModule,
        // BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        TimezonePickerModule,
        UserRoutingModule,
        NgxCaptchaModule,
        ToastrModule.forRoot(), // ToastrModule added
        JasperoConfirmationsModule,
        MatInputModule,
        NgxMaskModule.forRoot({})
    ],
    exports: [LoginComponent]
})
export class UsersModule {
}
