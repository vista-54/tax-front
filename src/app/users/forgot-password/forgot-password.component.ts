import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import * as moment from 'moment';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';

declare const document;

@Component({
  selector: 'app-login',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private _authService: AuthService,
              private toastrService: ToastrService) {
    this.forgotForm = this.formBuilder.group({
      email: ['', Validators.required, Validators.email]
    })
    ;

  }

  ngOnInit() {

  }

  get email() {
    return this.forgotForm.controls;
  }

  forgotPassSend() {
    console.log(this.email.email.value);
    this._authService.forgotPass(this.email.email.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log(data['message']);
          this.toastrService.success(data['message'], 'Success!');
        },
        error => {
          console.log(error);
          if (error.status == 401)
            this.toastrService.error('Invalid Email', 'Error!');
          this.error = 'Invalid Email';
          this.loading = false;
        });
  }

  back() {
    this.router.navigate(['/login']);
  }

}
