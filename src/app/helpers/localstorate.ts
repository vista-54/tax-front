import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageInfo {

    constructor() { }
    getAllInfo = () => {
        let aitax_online = {
            "introduction": JSON.parse(localStorage.getItem('introduction')),
            "personal_information": JSON.parse(localStorage.getItem('personal_information')),
            "spouse_information": JSON.parse(localStorage.getItem('spouse_information')),
            "spouse_additional_info": JSON.parse(localStorage.getItem('spouse_additional_info')),
            "cohan_rule": JSON.parse(localStorage.getItem('cohan_rule')),
            "basic_information": JSON.parse(localStorage.getItem('basic_information')),
            "dependent_information": JSON.parse(localStorage.getItem('dependent_information')),
            "fbar_information": JSON.parse(localStorage.getItem('fbar_information')),
            "rental_property": JSON.parse(localStorage.getItem('rental_property')),
            "side_income": JSON.parse(localStorage.getItem('side_income')),
            "charitable_donations": JSON.parse(localStorage.getItem('charitable_donations')),
            "property_donations": JSON.parse(localStorage.getItem('property_donations')),
            "volunter_expenses": JSON.parse(localStorage.getItem('volunter_expenses')),
            "tuition_related_expenses": JSON.parse(localStorage.getItem('tuition_related_expenses')),
            "tuition_loan_expenses": JSON.parse(localStorage.getItem('tuition_loan_expenses')),
            "all_adoption_expenses": JSON.parse(localStorage.getItem('all_adoption_expenses')),
            "home_expenses": JSON.parse(localStorage.getItem('home_expenses')),
            "small_items": JSON.parse(localStorage.getItem('small_items')),
            "business_expenses": JSON.parse(localStorage.getItem('business_expenses')),
            "vehicle_expenses": JSON.parse(localStorage.getItem('vehicle_expenses')),
            "vehicle_maintanence_expenses": JSON.parse(localStorage.getItem('vehicle_maintanence_expenses')),
            "vehicle_insurance": JSON.parse(localStorage.getItem('vehicle_insurance')),
            "all_entertainment_expenses": JSON.parse(localStorage.getItem('all_entertainment_expenses')),
        }
        let all_info = {'user_id': localStorage.getItem('user_id'), 'aitax_online': aitax_online}
        return all_info;
    }
}