import { TestBed } from '@angular/core/testing';

import { LlcIncformationService } from './llc-incformation.service';

describe('LlcIncformationService', () => {
  let service: LlcIncformationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LlcIncformationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
