import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLLCComponent } from './add-llc/add-llc.component';


const routes: Routes = [
  {
    path: '',
    component: AddLLCComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LlcInformationRoutingModule { }