import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddLLCComponent } from './add-llc.component';

describe('AddLLCComponent', () => {
  let component: AddLLCComponent;
  let fixture: ComponentFixture<AddLLCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLLCComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLLCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
