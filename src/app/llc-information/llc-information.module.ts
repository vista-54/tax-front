import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddLLCComponent } from './add-llc/add-llc.component';
import { LlcInformationRoutingModule } from './llc-information.routing.module';



@NgModule({
  declarations: [AddLLCComponent],
  imports: [
    CommonModule,
    LlcInformationRoutingModule
  ]
})
export class LlcInformationModule { }
