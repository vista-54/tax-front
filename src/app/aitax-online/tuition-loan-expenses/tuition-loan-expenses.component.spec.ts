import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TuitionLoanExpensesComponent } from './tuition-loan-expenses.component';

describe('TuitionLoanExpensesComponent', () => {
  let component: TuitionLoanExpensesComponent;
  let fixture: ComponentFixture<TuitionLoanExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TuitionLoanExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TuitionLoanExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
