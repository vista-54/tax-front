import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-tuition-loan-expenses',
  templateUrl: './tuition-loan-expenses.component.html',
  styleUrls: ['./tuition-loan-expenses.component.css']
})
export class TuitionLoanExpensesComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Education and Tuition-Related Expenses';
  tuitionLoanExpForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(15);
  }

  ngOnInit() {
    super.ngOnInit();
    this.tuitionLoanExpForm = this.formBuilder.group({
      se_load_checkbox: ['', Validators.required],
      loan_expenses: this.formBuilder.array([]),
    });

    const storedInfo = localStorage.getItem('tuition_loan_expenses');
    if (storedInfo) {
      this.tuitionLoanExpForm.patchValue(JSON.parse(storedInfo));
    }

    if (this.tuitionLoanExpForm.get('se_load_checkbox').value == '1') {
      // ;
      this.createExepense();
    }
  }

  newDonation(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get tutionLoanExpenses(): FormArray {
    return this.tuitionLoanExpForm.get('loan_expenses') as FormArray;
  }

  addExpense(obj?) {
    // ;
    if (obj) {
      this.tutionLoanExpenses.push(this.newDonation(obj));
    } else {
      this.tutionLoanExpenses.push(this.newDonation());
    }

  }

  removeExpense(i: number) {
    this.tutionLoanExpenses.removeAt(i);
  }

  createExepense = () => {
    var savedTuitionExp = this.getSavedExpen();
    if (savedTuitionExp.length) {
      for (let x in savedTuitionExp) {
        this.addExpense(savedTuitionExp[x]);
      }
    } else {
      this.addExpense();
    }
  };

  getSavedExpen() {
    let tuitionLoanExpInfo = localStorage.getItem('tuition_loan_expenses');
    tuitionLoanExpInfo = JSON.parse(tuitionLoanExpInfo);
    if (tuitionLoanExpInfo) {

      return tuitionLoanExpInfo['loan_expenses'];
    }
    return [];
  }

  onSubmit() {
    // stop here if form is invalid
    console.log(this.tuitionLoanExpForm.value);
    if (this.tuitionLoanExpForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('tuition_loan_expenses', JSON.stringify(this.tuitionLoanExpForm.value));

      this.aiTaxOnlineService.saveTuitionExpenses(this.tuitionLoanExpForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Tuition Loan Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/adoption-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.toastrService.warning('Please, fill all fields. All fields are required', 'Warning!');
      this.aiTaxOnlineService.touchAllFields(this.tuitionLoanExpForm);
    }
  }

  back() {
    localStorage.setItem('tuition_loan_expenses', JSON.stringify(this.tuitionLoanExpForm.value));
    this.router.navigate(['aitax-online/tuition-related-expenses']);
  }

  toggleDependentsInfo(e) {
    // console.log(e.target.value);
    if (e.target.value === '1') {
      this.createExepense();
    } else {
      this.clearFormArray(<FormArray>this.tuitionLoanExpForm.get('loan_expenses'));
    }
  }

  clearFormArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

}
