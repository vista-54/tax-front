import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first, mergeMap} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";
import {from} from "rxjs";

@Component({
  selector: 'app-charitable-donations',
  templateUrl: './charitable-donations.component.html',
  styleUrls: ['./charitable-donations.component.css']
})
export class CharitableDonationsComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Charitable Donations';
  charitableDonationsForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  calculation = 0;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService)
    progress.changeProgress(11);
  }

  ngOnInit() {
    super.ngOnInit();
    this.charitableDonationsForm = this.formBuilder.group({
      make_cache: ['', Validators.required],
      donations: this.formBuilder.array([]),
      pd_checkbox: ['', Validators.required],
      property_donations: this.formBuilder.array([]),
      ve_volunteering_checkbox: ['', Validators.required],
      expenses: this.formBuilder.array([]),
    });

    const storedInfoDonations = localStorage.getItem('charitable_donations');
    if (storedInfoDonations) {
      this.charitableDonationsForm.patchValue(JSON.parse(storedInfoDonations));
    }
    const storedPropertyInfoDonations = localStorage.getItem('property_donations');
    if (storedPropertyInfoDonations) {
      this.charitableDonationsForm.patchValue(JSON.parse(storedPropertyInfoDonations));
    }
    const storedInfoExpensesDonations = localStorage.getItem('volunter_expenses');
    if (storedInfoExpensesDonations) {
      this.charitableDonationsForm.patchValue(JSON.parse(storedInfoExpensesDonations));
    }
    if (this.charitableDonationsForm.get('make_cache').value == '1') {
      this.createDonations();
      this.calc();
    }
    if (this.charitableDonationsForm.get('pd_checkbox').value == '1') {
      this.createPropertyDonations();
    }
    if (this.charitableDonationsForm.get('ve_volunteering_checkbox').value == '1') {
      this.createExpense();

    }
  }

  /**
   * New property
   * @param obj
   */
  newDonations(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        description: [obj ? obj.description : ''],
        frequency: [obj ? obj.frequency : ''],
        amount: [obj ? obj.amount : '']
      });
    } else {
      return this.formBuilder.group({
        description: ['', Validators.required],
        frequency: ['', Validators.required],
        amount: ['', Validators.required]
      });
    }

  }

  newPropertyDonations(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
        cost_value: [obj ? obj.cost_value : '',[Validators.required]],
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required],
      });
    }
  }

  newExpense(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]],
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required],
      });
    }
  }

  get donations(): FormArray {
    return this.charitableDonationsForm.get('donations') as FormArray;
  }

  get property_donations(): FormArray {
    return this.charitableDonationsForm.get('property_donations') as FormArray;
  }

  get expenses(): FormArray {
    return this.charitableDonationsForm.get('expenses') as FormArray;
  }

  /**
   * Add donations to arrays
   * @param obj
   */
  addDonations(obj?) {
    // ;
    if (obj) {
      this.donations.push(this.newDonations(obj));
    } else {
      this.donations.push(this.newDonations());
    }

  }

  addPropertyDonations(obj?) {
    // ;
    if (obj) {
      this.property_donations.push(this.newPropertyDonations(obj));
    } else {
      this.property_donations.push(this.newPropertyDonations());
    }
  }

  addExpense(obj?) {
    // ;
    if (obj) {
      this.expenses.push(this.newExpense(obj));
    } else {
      this.expenses.push(this.newExpense());
    }

  }

  /**
   * Remove Properties
   * @param i
   */
  removeDonations(i: number) {
    this.donations.removeAt(i);
  }

  removePropertyDonations(i: number) {
    this.property_donations.removeAt(i);
  }

  removeExpenses(i: number) {
    this.expenses.removeAt(i);
  }

  /**
   * Create Child
   */
    //Donation
  createDonations = () => {
    let savedDonations = this.getSavedDonations();
    if (savedDonations.length) {
      for (let x in savedDonations) {
        this.addDonations(savedDonations[x]);
        console.log(savedDonations[x]);
      }
    } else {
      this.addDonations();
    }
  };

  /*Property donations*/

  createPropertyDonations = () => {
    let savedDonations = this.getSavedPropertyDonations();
    if (savedDonations.length) {
      for (let x in savedDonations) {
        this.addPropertyDonations(savedDonations[x]);
        console.log(savedDonations[x]);
      }
    } else {
      this.addPropertyDonations();
    }
  };

  createExpense = () => {
    let savedDonations = this.getSavedExpenses();
    if (savedDonations.length) {
      for (let x in savedDonations) {
        this.addExpense(savedDonations[x]);
        console.log(savedDonations[x]);
      }
    } else {
      this.addExpense();
    }
  };

  /**
   * Get Saved
   */
  getSavedDonations() {
    let savedDonations = localStorage.getItem('charitable_donations');
    savedDonations = JSON.parse(savedDonations);
    if (savedDonations) {
      console.log(savedDonations['donations']);
      return savedDonations['donations'];
    }
    return [];
  }

  getSavedPropertyDonations() {
    let savedDonations = localStorage.getItem('property_donations');
    savedDonations = JSON.parse(savedDonations);
    if (savedDonations) {
      console.log(savedDonations['property_donations']);
      return savedDonations['property_donations'];
    }
    return [];
  }

  getSavedExpenses() {
    let savedDonations = localStorage.getItem('volunter_expenses');
    savedDonations = JSON.parse(savedDonations);
    if (savedDonations) {
      console.log(savedDonations['expenses']);
      return savedDonations['expenses'];
    }
    return [];
  }

  /**
   * Toggles
   * @param e
   */
  toggleDonationsInfo(e) {
    if (e.value == '1') {
      this.createDonations();
    } else {
      this.clearDonations(<FormArray>this.charitableDonationsForm.get('donations'));
    }
  }

  togglePropertyDonationsInfo(e) {
    if (e.value == '1') {
      this.createPropertyDonations();
    } else {
      this.clearPropertyDonations(<FormArray>this.charitableDonationsForm.get('property_donations'));
    }
  }

  toggleExpenseInfo(e) {
    if (e.value == '1') {
      this.createExpense();
    } else {
      this.clearExpense(<FormArray>this.charitableDonationsForm.get('expenses'));
    }
  }

  /**
   * Clean Properties
   * @param formArray
   */

  clearDonations = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }
  };

  clearPropertyDonations = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }
  };
  clearExpense = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }
  };

  onSubmit() {
    console.log(this.charitableDonationsForm.value);
    if (!this.aiTaxOnlineService.validate(this.charitableDonationsForm)) {
      return false;
    }
    // stop here if form is invalid
    if (this.charitableDonationsForm.valid) {
      this.submitted = true;
      this.loading = true;
      if (this.charitableDonationsForm.get('make_cache').value === '0') {
        this.charitableDonationsForm.removeControl('frequency');
        this.charitableDonationsForm.removeControl('amount');
      }
      localStorage.setItem('charitable_donations', JSON.stringify({
        make_cache: this.charitableDonationsForm.get('make_cache').value,
        donations: this.charitableDonationsForm.get('donations').value
      }));
      localStorage.setItem('property_donations', JSON.stringify({
        pd_checkbox: this.charitableDonationsForm.get('pd_checkbox').value,
        property_donations: this.charitableDonationsForm.get('property_donations').value
      }));
      localStorage.setItem('volunter_expenses', JSON.stringify({
        ve_volunteering_checkbox: this.charitableDonationsForm.get('ve_volunteering_checkbox').value,
        expenses: this.charitableDonationsForm.get('expenses').value
      }));

      from([{
        request: 'char_donation',
        make_cache: this.charitableDonationsForm.get('make_cache').value,
        donations: this.charitableDonationsForm.get('donations').value
      },
        {
          request: 'prop_donation',
          pd_checkbox: this.charitableDonationsForm.get('pd_checkbox').value,
          donations: this.charitableDonationsForm.get('property_donations').value
        }
        ,
        {
          request: 'vol_exp',
          ve_volunteering_checkbox: this.charitableDonationsForm.get('ve_volunteering_checkbox').value,
          expenses: this.charitableDonationsForm.get('expenses').value
        }
      ])
        .pipe(
          mergeMap(param => this.sendToServer(param)),
          first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Donations Information saved successfully', 'Success!');
            this.router.navigate(['aitax-online/tuition-related-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });

    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.charitableDonationsForm);
    }
  }

  back() {
    localStorage.setItem('charitable_donations', JSON.stringify({
      make_cache: this.charitableDonationsForm.get('make_cache').value,
      donations: this.charitableDonationsForm.get('donations').value
    }));
    localStorage.setItem('property_donations', JSON.stringify({
      pd_checkbox: this.charitableDonationsForm.get('pd_checkbox').value,
      property_donations: this.charitableDonationsForm.get('property_donations').value
    }));
    localStorage.setItem('volunter_expenses', JSON.stringify({
      ve_volunteering_checkbox: this.charitableDonationsForm.get('ve_volunteering_checkbox').value,
      expenses: this.charitableDonationsForm.get('expenses').value
    }));
    this.router.navigate(['aitax-online/side-income-information']);
  }

  calc() {
    let calc = 0;
    let arr = this.charitableDonationsForm.get('donations').value;
    arr.map((item, index, array) => {
      calc += (+item.frequency * +item.amount);
    });
    this.calculation = calc;
  }


  sendToServer(obj) {
    switch (obj.request) {
      case 'char_donation':
        return this.aiTaxOnlineService.saveCharitableDonations(obj);

      case 'prop_donation':
        return this.aiTaxOnlineService.savePropertyDonations(obj);

      case 'vol_exp':
        return this.aiTaxOnlineService.saveVolExp(obj);

    }
  }
}
