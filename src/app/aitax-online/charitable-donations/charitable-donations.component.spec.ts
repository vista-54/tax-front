import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharitableDonationsComponent } from './charitable-donations.component';

describe('CharitableDonationsComponent', () => {
  let component: CharitableDonationsComponent;
  let fixture: ComponentFixture<CharitableDonationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharitableDonationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharitableDonationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
