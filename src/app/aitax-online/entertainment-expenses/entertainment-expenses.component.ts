import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
    selector: 'app-entertainment-expenses',
    templateUrl: './entertainment-expenses.component.html',
    styleUrls: ['./entertainment-expenses.component.css']
})
export class EntertainmentExpensesComponent extends ActivityTimeTracker  implements OnInit {
    section_name = 'Meals & Entertainment';
    entertainmentExpForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';
    success = '';
    isMarried: boolean = false;

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                public aiTaxOnlineService: AITaxOnlineService,
                private progress: ProgressService,
                private toastrService: ToastrService) {
      super(aiTaxOnlineService)
      progress.changeProgress(21);
    }

    ngOnInit() {
      super.ngOnInit()
        this.entertainmentExpForm = this.formBuilder.group({
            eme_time_per_week_radio: ['', Validators.required],
            eme_time_per_week: [''],
            eme_meal_cost: [''],
            eme_spouse_time_per_week: [''],
            eme_spouse_meal_cost: [''],
            eme_gift_Description_radio: ['', Validators.required],
            entertainment_expenses: this.formBuilder.array([]),
        });
        const personalInfo = localStorage.getItem('personal_information');
        if (personalInfo) {
            this.isMarried = JSON.parse(personalInfo).marital_status === '1';
        }
        const storedInfo = localStorage.getItem('all_entertainment_expenses');
        if (storedInfo) {
            this.entertainmentExpForm.patchValue(JSON.parse(storedInfo));
        }

        if (this.entertainmentExpForm.get('eme_gift_Description_radio').value === '1') {
            // ;
            this.createExepense();
        }
    }

    newDonation(obj?): FormGroup {
        if (obj) {
            return this.formBuilder.group({
                cost_description: [obj ? obj.cost_description : '',[Validators.required]],
                 cost_value: [obj ? obj.cost_value: '',[Validators.required]]
            });
        } else {
            return this.formBuilder.group({
                cost_description: ['', Validators.required],
                cost_value: ['', Validators.required]
            });
        }

    }

    get tutionExpenses(): FormArray {
        return this.entertainmentExpForm.get('entertainment_expenses') as FormArray;
    }

    addExpense(obj?) {
        // ;
        if (obj) {
            this.tutionExpenses.push(this.newDonation(obj));
        } else {
            this.tutionExpenses.push(this.newDonation());
        }

    }

    removeExpense(i: number) {
        this.tutionExpenses.removeAt(i);
    }

    createExepense = () => {
        var savedTuitionExp = this.getSavedEntertainmentExp();
        if (savedTuitionExp.length) {
            for (let x in savedTuitionExp) {
                this.addExpense(savedTuitionExp[x]);
            }
        } else {
            this.addExpense();
        }
    };

    getSavedEntertainmentExp() {
        let storedEntertainmentExp = localStorage.getItem('all_entertainment_expenses');
        storedEntertainmentExp = JSON.parse(storedEntertainmentExp);
        if (storedEntertainmentExp) {

            return storedEntertainmentExp['entertainment_expenses'];
        }
        return [];
    }

    onSubmit() {
        // stop here if form is invalid
        console.log(this.entertainmentExpForm.value);
      if (!this.aiTaxOnlineService.validate(this.entertainmentExpForm)) {
        return false;
      }
        if (this.entertainmentExpForm.valid) {
            this.submitted = true;
            this.loading = true;
            localStorage.setItem('all_entertainment_expenses', JSON.stringify(this.entertainmentExpForm.value));

            this.aiTaxOnlineService.saveEntertainmentExp(this.entertainmentExpForm.value)
                .pipe(first())
                .subscribe(
                    data => {
                        this.submitted = true;
                        this.loading = false;
                        this.toastrService.success('Entertainment Expense Information saved successfully.', 'Success!');
                        this.router.navigate(['aitax-online/work-tools']);
                    },
                    errorRes => {
                        this.toastrService.error(errorRes.error.message, 'Error!');
                        this.loading = false;
                        this.submitted = false;
                    });
        } else {
            this.loading = false;
            this.submitted = false;
            this.error = '';
            // alert('invalid');
            this.aiTaxOnlineService.touchAllFields(this.entertainmentExpForm);
        }
    }

    back() {
      localStorage.setItem('all_entertainment_expenses', JSON.stringify(this.entertainmentExpForm.value));
      this.router.navigate(['aitax-online/additional-transportation-questions']);
    }

    toggleDependentsInfo(e) {
        // console.log(e.target.value);
        if (e.value == '1') {
            this.createExepense();
        } else {
            this.clearFormArray(<FormArray>this.entertainmentExpForm.get('entertainment_expenses'));
        }
    }

    clearFormArray = (formArray: FormArray) => {
        if (formArray && formArray.length) {
            while (formArray.length !== 0) {
                formArray.removeAt(0);
            }
        }

    };
}
