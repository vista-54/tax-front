import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntertainmentExpensesComponent } from './entertainment-expenses.component';

describe('EntertainmentExpensesComponent', () => {
  let component: EntertainmentExpensesComponent;
  let fixture: ComponentFixture<EntertainmentExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntertainmentExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntertainmentExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
