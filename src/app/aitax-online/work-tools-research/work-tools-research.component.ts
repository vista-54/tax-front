import {Component, OnInit} from '@angular/core';
import {AITaxOnlineService} from '../aitax-online.service';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-work-tools-research',
  templateUrl: './work-tools-research.component.html',
  styleUrls: ['./work-tools-research.component.css']
})
export class WorkToolsResearchComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Work Tools & Misc'
  workToolsResearchForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  modalRef: NgbModalRef;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private modalService: NgbModal,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService)
    progress.changeProgress(26);
  }

  ngOnInit() {
    super.ngOnInit()
    this.workToolsResearchForm = this.formBuilder.group({
      research_radio: ['', Validators.required],
      research: this.formBuilder.array([]),
      internet_pay: ['', Validators.required]
    });

    const storedInfo = localStorage.getItem('work_tools_research');
    if (storedInfo) {
      this.workToolsResearchForm.patchValue(JSON.parse(storedInfo));
    }
    if (this.workToolsResearchForm.get('research_radio').value == '1') {
      this.createResearch();
    }
  }


  newResearch(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: '',
        cost_value: ''
      });
    }

  }

  get research(): FormArray {
    return this.workToolsResearchForm.get('research') as FormArray;
  }

  addResearch(obj?) {
    // ;
    if (obj) {
      this.research.push(this.newResearch(obj));
    } else {
      this.research.push(this.newResearch());
    }

  }

  removeResearch(i: number) {
    this.research.removeAt(i);
  }

  createResearch = () => {
    let savedResearch = this.getSavedResearch();
    if (savedResearch.length) {
      for (let x in savedResearch) {
        this.addResearch(savedResearch[x]);
      }
    } else {
      this.addResearch();
    }
  };

  getSavedResearch() {
    let savedResearch = localStorage.getItem('work_tools_research');
    savedResearch = JSON.parse(savedResearch);
    if (savedResearch) {
      return savedResearch['research'];
    }
    return [];
  }

  toggleResearchInfo(e) {
    // console.log(e.target.value);
    if (e.target.value == '1') {
      this.createResearch();
    } else {
      this.clearResearch(<FormArray>this.workToolsResearchForm.get('research'));
    }
  }

  clearResearch = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }
  };


  onSubmit() {
    // stop here if form is invalid
    console.log(this.workToolsResearchForm.value);
    if (this.workToolsResearchForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('work_tools_research', JSON.stringify(this.workToolsResearchForm.value));

      this.aiTaxOnlineService.saveWorkToolsResearchInfo(this.workToolsResearchForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Work Tools Research Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/blue-collar-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.workToolsResearchForm);
    }
  }

  back() {
    localStorage.setItem('work_tools_research', JSON.stringify(this.workToolsResearchForm.value));
    this.router.navigate(['aitax-online/work-tools-exam']);
  }


  openModal = (modelId): void => {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      size: 'md',
      windowClass: 'modal_extraDOc',
    };
    this.modalRef = this.modalService.open(modelId, ngbModalOptions);
  };

}
