import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {USA_STATES} from '../../shared/constants/usa.states';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-basic-information',
  templateUrl: './basic-information.component.html',
  styleUrls: ['./basic-information.component.css']
})
export class BasicInformationComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Basic Information';
  public states: string[] = [];
  basicInfoForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(6);
  }

  ngOnInit() {
    super.ngOnInit();
    this.basicInfoForm = this.formBuilder.group({
      city: ['', Validators.required],
      state: ['', Validators.required],
      school: ['', Validators.required],
      county: ['', Validators.required]
    });
    const storedInfo = localStorage.getItem('basic_information');
    if (storedInfo) {
      this.basicInfoForm.patchValue(JSON.parse(storedInfo));
    }
    this.states = USA_STATES;

  }

  onSubmit() {

    this.aiTaxOnlineService.validate(this.basicInfoForm)
    // stop here if form is invalid
    console.log(this.basicInfoForm.value);
    if (this.basicInfoForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('basic_information', JSON.stringify(this.basicInfoForm.value));

      this.aiTaxOnlineService.saveBasicInfoForm(this.basicInfoForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Basic Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/dependent-information']);
          },
          errorRes => {

            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.basicInfoForm);
    }
  }

  back() {
    localStorage.setItem('basic_information', JSON.stringify(this.basicInfoForm.value));
    this.router.navigate(['aitax-online/cohan-rule']);
  }
}
