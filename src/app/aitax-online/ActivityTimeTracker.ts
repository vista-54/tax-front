import {OnDestroy, OnInit} from "@angular/core";
import {AITaxOnlineService} from "./aitax-online.service";
import {delay} from "rxjs/operators";
import {FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";


export class ActivityTimeTracker implements OnInit, OnDestroy {

  protected section_name: string;


  constructor(public aiTaxOnlineService: AITaxOnlineService) {
  }

  ngOnInit() {
    console.log(`${this.section_name} started`);
    this.aiTaxOnlineService.sectionStart({'section_name': this.section_name})
      .subscribe()
  }

  ngOnDestroy() {
    console.log(`${this.section_name} finished`);
    this.aiTaxOnlineService.sectionFinish({'section_name': this.section_name})
      .pipe(delay(2000))
      .subscribe()
  }


}
