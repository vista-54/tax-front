import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';

import {NgbModalRef, NgbModal, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";


@Component({
  selector: 'app-additional-screen',
  templateUrl: './deposit-information.component.html',
  styleUrls: ['./deposit-information.component.css']
})
export class DepositInformationComponent extends ActivityTimeTracker implements OnInit {

  section_name = 'Direct Deposit Information';
  depositInformationForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  modalRef: NgbModalRef;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public aiTaxOnlineService: AITaxOnlineService,
    private progress: ProgressService,
    private modalService: NgbModal,
    private toastrService: ToastrService
  ) {
    super(aiTaxOnlineService)
    progress.changeProgress(28);
  }

  ngOnInit() {
    super.ngOnInit()
    this.depositInformationForm = this.formBuilder.group({
      refund_deposit: ['', Validators.required],
      refund_between_acc: [''],
      bank_routing: [''],
      bank_name: [''],
      account_number: [''],
      account_owner: [''],
      secondary_bank: [''],
      secondary_account_owner: [''],
      mailing_address: [''],
      swift: [''],
      bsb_number: [''],
      address_record: [''],
      address_record_than: [''],
      address: [''],
      city: [''],
      state: [''],
      postal_code: [''],
      country: [''],
    });

    let storedInfo = localStorage.getItem('deposit_information');
    if (storedInfo) {
      this.depositInformationForm.patchValue(JSON.parse(storedInfo));
    }


  }

  onSubmit() {
    console.log(this.depositInformationForm.value);
    if (this.depositInformationForm.valid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('deposit_information', JSON.stringify(this.depositInformationForm.value));

      this.aiTaxOnlineService.saveDepositInformation(this.depositInformationForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Deposit Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/conclusion']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      this.aiTaxOnlineService.touchAllFields(this.depositInformationForm);
    }
  }

  back() {
    localStorage.setItem('deposit_information', JSON.stringify(this.depositInformationForm.value));
    this.router.navigate(['aitax-online/pocket-medical-expenses-2']);
  }
}
