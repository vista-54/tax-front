import { TestBed } from '@angular/core/testing';

import { AITaxOnlineService } from './aitax-online.service';

describe('AITaxOnlineService', () => {
  let service: AITaxOnlineService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AITaxOnlineService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
