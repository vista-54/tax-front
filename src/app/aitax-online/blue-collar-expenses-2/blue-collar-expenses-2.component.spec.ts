import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlueCollarExpenses2Component } from './blue-collar-expenses-2.component';

describe('EmergencyPersonalExpensesComponent', () => {
  let component: BlueCollarExpenses2Component;
  let fixture: ComponentFixture<BlueCollarExpenses2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlueCollarExpenses2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlueCollarExpenses2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
