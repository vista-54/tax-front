import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first, mergeMap} from 'rxjs/operators';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";
import {from} from "rxjs";

@Component({
  selector: 'app-blue-collar-expenses-2',
  templateUrl: './blue-collar-expenses-2.component.html',
  styleUrls: ['./blue-collar-expenses-2.component.css']
})
export class BlueCollarExpenses2Component extends ActivityTimeTracker implements OnInit {
  section_name = 'Blue Collar Expenses';
  blueCollarExpensesForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  modalRef: NgbModalRef;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private modalService: NgbModal,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService)
    progress.changeProgress(25);
  }

  ngOnInit() {
    super.ngOnInit()
    this.blueCollarExpensesForm = this.formBuilder.group({
      expenses_uniforms_radio: ['', Validators.required],
      expenses_uniforms: this.formBuilder.array([]),
      work_clothing_radio: ['', Validators.required],
      work_clothing: this.formBuilder.array([]),
      dry_cleaning_radio: ['', Validators.required],
      dry_cleaning: this.formBuilder.array([]),
      expenses_equipment_radio: ['', Validators.required],
      expenses_equipment: this.formBuilder.array([]),
    });

    const storedInfo = localStorage.getItem('blue_collar_expenses_2');
    if (storedInfo) {
      this.blueCollarExpensesForm.patchValue(JSON.parse(storedInfo));
    }

    if (this.blueCollarExpensesForm.get('expenses_uniforms_radio').value === '1') {
      // ;
      this.createUniforms();
    }
    if (this.blueCollarExpensesForm.get('work_clothing_radio').value === '1') {
      // ;
      this.createWorkClothing();
    }
    if (this.blueCollarExpensesForm.get('dry_cleaning_radio').value === '1') {
      // ;
      this.createDryCleaning();
    }
    debugger
    if (this.blueCollarExpensesForm.get('expenses_equipment_radio').value == '1') {
      // ;
      this.createEquipments();
    }
  }

  newDryCleaning(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description:['',[Validators.required]],
        cost_value: ['',[Validators.required]]
      });
    }

  }

  get dryCleaning(): FormArray {
    return this.blueCollarExpensesForm.get('dry_cleaning') as FormArray;
  }

  removeEquipments(i: number) {
    this.equipments.removeAt(i);
  }

  addDryCleaning(obj?) {
    // ;
    if (obj) {
      this.dryCleaning.push(this.newDryCleaning(obj));
    } else {
      this.dryCleaning.push(this.newDryCleaning());
    }

  }

  removeDryCleaning(i: number) {
    this.dryCleaning.removeAt(i);
  }

  createDryCleaning = () => {
    let savedDryCleanings = this.getSavedDryCleaning();
    if (savedDryCleanings.length) {
      for (let x in savedDryCleanings) {
        this.addDryCleaning(savedDryCleanings[x]);
      }
    } else {
      this.addDryCleaning();
    }
  };

  getSavedDryCleaning() {
    let expInformation = localStorage.getItem('blue_collar_expenses_2');
    expInformation = JSON.parse(expInformation);
    if (expInformation) {

      return expInformation['dry_cleaning'];
    }
    return [];
  }

  toggleDryCleaningInfo(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createDryCleaning();
    } else {
      this.clearExpArray(<FormArray>this.blueCollarExpensesForm.get('dry_cleaning'));
    }
  }

  clearExpArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  /** Equibment */
  newWorkClothing(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['',[Validators.required]],
        cost_value: ['',[Validators.required]]
      });
    }

  }

  get workClothing(): FormArray {
    return this.blueCollarExpensesForm.get('work_clothing') as FormArray;
  }

  addWorkClothing(obj?) {
    // ;
    if (obj) {
      this.workClothing.push(this.newWorkClothing(obj));
    } else {
      this.workClothing.push(this.newWorkClothing());
    }

  }

  removeWorkClothing(i: number) {
    this.workClothing.removeAt(i);
  }

  createWorkClothing = () => {
    let savedUniforms = this.getSavedWorkClothing();
    if (savedUniforms.length) {
      for (let x in savedUniforms) {
        this.addWorkClothing(savedUniforms[x]);
      }
    } else {
      this.addWorkClothing();
    }
  };

  getSavedWorkClothing() {
    let savedUniforms = localStorage.getItem('blue_collar_expenses_2');
    savedUniforms = JSON.parse(savedUniforms);
    if (savedUniforms) {

      return savedUniforms['work_clothing'];
    }
    return [];
  }

  toggleEquipmentInfo(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createWorkClothing();
    } else {
      this.clearWorkClothing(<FormArray>this.blueCollarExpensesForm.get('work_clothing'));
    }
  }

  clearWorkClothing = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };


  /** Uniforms */
  newUniforms(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['',[Validators.required]],
        cost_value: ['',[Validators.required]]
      });
    }

  }

  get uniforms(): FormArray {
    return this.blueCollarExpensesForm.get('expenses_uniforms') as FormArray;
  }

  addUniforms(obj?) {
    // ;
    if (obj) {
      this.uniforms.push(this.newUniforms(obj));
    } else {
      this.uniforms.push(this.newUniforms());
    }

  }

  removeUniforms(i: number) {
    this.uniforms.removeAt(i);
  }

  createUniforms = () => {
    let savedUniforms = this.getSavedUniforms();
    if (savedUniforms.length) {
      for (let x in savedUniforms) {
        this.addUniforms(savedUniforms[x]);
      }
    } else {
      this.addUniforms();
    }

  };

  getSavedUniforms() {
    let savedUniforms = localStorage.getItem('blue_collar_expenses_2');
    savedUniforms = JSON.parse(savedUniforms);
    if (savedUniforms) {

      return savedUniforms['expenses_uniforms'];
    }
    return [];
  }

  toggleExpensesUniforms(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createUniforms();
    } else {
      this.clearUniformsArray(<FormArray>this.blueCollarExpensesForm.get('expenses_uniforms'));
    }
  }

  toggleExpensesEquipments(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createEquipments();
    } else {
      this.clearUniformsArray(<FormArray>this.blueCollarExpensesForm.get('expenses_equipment'));
    }
  }

  get equipments(): FormArray {
    return this.blueCollarExpensesForm.get('expenses_equipment') as FormArray;
  }

  createEquipments = () => {
    let savedEquipments = this.getSavedEquipments();
    if (savedEquipments.length) {
      for (let x in savedEquipments) {
        this.addEquipments(savedEquipments[x]);
      }
    } else {
      this.addEquipments();
    }
  };

  addEquipments(obj?) {
    if (obj) {
      this.equipments.push(this.newEquipments(obj));
    } else {
      this.equipments.push(this.newEquipments());
    }

  }

  /** Equipments */
  newEquipments(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['',[Validators.required]],
        cost_value: ['',[Validators.required]]
      });
    }

  }

  getSavedEquipments() {
    let savedEquipments = localStorage.getItem('blue_collar_expenses_2');
    console.log(savedEquipments);
    savedEquipments = JSON.parse(savedEquipments);
    if (savedEquipments) {

      return savedEquipments['expenses_equipment'];
    }
    return [];
  }

  clearUniformsArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  onSubmit() {
    if (!this.aiTaxOnlineService.validate(this.blueCollarExpensesForm)) {
      return false;
    }
    // stop here if form is invalid
    console.log(this.blueCollarExpensesForm.value);
    if (this.blueCollarExpensesForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      const blueCollarFirst = {
        request:'blue',
        expenses_equipment_radio: this.blueCollarExpensesForm.get('expenses_equipment_radio').value,
        expenses_equipment: this.blueCollarExpensesForm.get('expenses_equipment').value
      }

      from([blueCollarFirst, this.blueCollarExpensesForm.value])
        .pipe(
          mergeMap(param => this.sendToServer(param)),
          first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            localStorage.setItem('blue_collar_expenses_2', JSON.stringify(this.blueCollarExpensesForm.value));
            this.toastrService.success('Blue Collar Expenses Information saved successfully', 'Success!');
            this.router.navigate(['aitax-online/emergency-personal-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.blueCollarExpensesForm);
    }
  }

  back() {
    localStorage.setItem('blue_collar_expenses_2', JSON.stringify(this.blueCollarExpensesForm.value));
    this.router.navigate(['aitax-online/work-tools-exam']);
  }

  openModal = (modelId): void => {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      size: 'md',
      windowClass: 'modal_extraDOc',
    };
    this.modalRef = this.modalService.open(modelId, ngbModalOptions);
  };

  sendToServer(obj) {
    switch (obj.request) {
      case 'blue':
        return this.aiTaxOnlineService.saveBlueCollarExpensesInfo(obj);
      default:
        return this.aiTaxOnlineService.saveBlueCollarExpenses2Info(obj);
    }
  }
}
