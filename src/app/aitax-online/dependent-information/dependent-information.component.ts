import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";


@Component({
  selector: 'app-dependent-information',
  templateUrl: './dependent-information.component.html',
  styleUrls: ['./dependent-information.component.css']
})
export class DependentInformationComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Dependent Information';
  dependentForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  storedInfo: any;
  date_now: any;
  _equals: any;
  arrayItems: {
    id: number;
    full_legal_name: string
    date_of_birth: Date;
    relation: string;
    number_of_months_lived: string;
    married_as: Date;
    ssn: string;
    college_student: boolean;
    recieve_amount_greater_than_4050: boolean;
  }[];

  days = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  year = [];


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(7);
  }

  ngOnInit() {
    super.ngOnInit();
    this.dependentForm = this.formBuilder.group({
      have_dependents: ['', Validators.required],
      dependents: this.formBuilder.array([]),
    });
    this.storedInfo = localStorage.getItem('dependent_information');
    if (this.storedInfo) {
      this.dependentForm.patchValue(JSON.parse(this.storedInfo));
    }

    if (this.dependentForm.get('have_dependents').value == '1') {
      // debugger;
      this.createDependents();
    }
    for (let i = new Date().getFullYear(); i >= 1930; i--) {
      this.year.push(i);
    }
    this.date_now = +new Date() * 1;
    // this.addDependent();
  }

  newDependent(obj?): FormGroup {
    // debugger;
    if (obj) {
      return this.formBuilder.group({
        full_legal_name: [obj ? obj.full_legal_name : '', [Validators.required]],
        date_of_birth: [obj ? obj.date_of_birth : '', [Validators.required]],
        relation: [obj ? obj.relation : '', [Validators.required]],
        month_lived: [obj ? obj.month_lived : '', [Validators.required]],
        married_as: [obj ? obj.married_as : '', [Validators.required]],
        ssn: [obj ? obj.ssn : '', [Validators.required]],
        fulltime_student: [obj ? obj.fulltime_student : '', [Validators.required]],
        received_income: [obj ? obj.received_income : '', [Validators.required]],
        day: [obj ? obj.day : '', [Validators.required]],
        month: [obj ? obj.month : '', [Validators.required]],
        year: [obj ? obj.year : '', [Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        full_legal_name: ['', Validators.required],
        date_of_birth: ['', Validators.required],
        relation: ['', Validators.required],
        month_lived: ['', Validators.required],
        married_as: ['', Validators.required],
        ssn: ['', Validators.required],
        fulltime_student: ['', Validators.required],
        received_income: ['', Validators.required],
        day: ['', [Validators.required]],
        month: ['', [Validators.required]],
        year: ['', [Validators.required]],
      });
    }

  }

  get dependents(): FormArray {
    return this.dependentForm.get('dependents') as FormArray;
  }

  addDependent(obj?) {
    // debugger;
    if (obj) {
      this.dependents.push(this.newDependent(obj));
    } else {
      this.dependents.push(this.newDependent());
    }

  }

  removeDependent(i: number) {
    this.dependents.removeAt(i);
  }

  createDependents = () => {
    const dependents = this.getSavedDependents();
    if (dependents) {
      if (dependents.length) {
        for (const x in dependents) {
          this.addDependent(dependents[x]);
        }
      } else {
        this.addDependent();
      }
    } else {
      this.addDependent();
    }

  };

  getSavedDependents() {
    let dependentsInformation = localStorage.getItem('dependent_information');
    dependentsInformation = JSON.parse(dependentsInformation);
    if (dependentsInformation) {
      return dependentsInformation['dependents'];
    } else {
      return null;
    }
  }

  onSubmit() {
    debugger
    let err = 0;
    let arr = this.dependentForm.get('dependents').value;
    arr.map((item, index, array) => {
      if (item['day'] !== '' && item['month'] !== '' && item['year'] !== '') {
        item['date_of_birth'] = '' + item['year'] + '-' + item['month'] + '-' + item['day'];
      } else {
        item['date_of_birth'] = '';
      }
      if (item['date_of_birth'] == '') {
        err++;
      }
    });
    if (err) {
      this.toastrService.warning('Please enter a valid date!', 'Warning!');
    } else {
      debugger
      // if (!this.registerForm.invalid) {
      console.log(this.dependentForm.value.dependents[0]);
      // if (this.dependentForm.valid) {
      if (!this.aiTaxOnlineService.validate(this.dependentForm)) {
        return false;
      }
      this.submitted = true;
      this.loading = true;
      // this.dependentForm.get('dependents').value[i]['date_of_birth']) * 1 > this.date_now
      console.log(this.dependentForm.value);
      localStorage.setItem('dependent_information', JSON.stringify(this.dependentForm.value));

      this.aiTaxOnlineService.saveDependentInfo(this.dependentForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.success = 'Personal Information updated successfully.';
            this.toastrService.success('Dependents Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/fbar-information']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');

            this.loading = false;
            this.submitted = false;
          });
      // } else {
      //   this.loading = false;
      //   this.submitted = false;
      //   this.error = '';
      //   // alert('invalid');
      //   this.aiTaxOnlineService.touchAllFields(this.dependentForm);
      // }

    }
  }


  back() {
    localStorage.setItem('dependent_information', JSON.stringify(this.dependentForm.value));
    let country = '';
    if (this.storedInfo.hasOwnProperty('country')) {
      country = this.storedInfo['country'];
    }
    if (country === 'United States') {
      this.router.navigate(['aitax-online/basic-information']);
    } else {
      this.router.navigate(['aitax-online/cohan-rule']);
    }
  }

  toggleDependentsInfo(e) {
    debugger
    if (e.value == '1') {
      this.createDependents();
    } else {
      this.clearFormArray(<FormArray>this.dependentForm.get('dependents'));
    }
  }

  clearFormArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  test(i) {
    let arr = this.dependentForm.get('dependents').value;
    let _day = arr[i]['day'];
    let _month = arr[i]['month'];
    let _year = arr[i]['year'];


    if (_day !== '' && _month !== '' && _year !== '') {
      let date = '' + _year + '-' + _month + '-' + _day;
      arr[i]['date_of_birth'] = date;
    }

    console.log(arr[0]['date_of_birth']);
  }

  // _equal(date) {
  //   return new Date(date);
  //
  //   // if (new Date() ) {
  //   //   return this._equals = false;
  //   // } else {
  //   //   return this._equals = true;
  //   // }
  // }

}
