import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalTransportationQuestionsComponent } from './additional-transportation-questions.component';

describe('AdditionalTransportationQuestionsComponent', () => {
  let component: AdditionalTransportationQuestionsComponent;
  let fixture: ComponentFixture<AdditionalTransportationQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdditionalTransportationQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalTransportationQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
