import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-additional-transportation-questions',
  templateUrl: './additional-transportation-questions.component.html',
  styleUrls: ['./additional-transportation-questions.component.css']
})
export class AdditionalTransportationQuestionsComponent extends ActivityTimeTracker  implements OnInit {
  section_name = 'Additional Transportation Questions'
  transportationForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public aiTaxOnlineService: AITaxOnlineService,
    private progress: ProgressService
  ) {
    super(aiTaxOnlineService)
    progress.changeProgress(20);
  }

  ngOnInit() {
    super.ngOnInit();
    this.transportationForm = this.formBuilder.group({
      distance: ['', Validators.required],
      days: ['', Validators.required],
      other_dist_checkbox: ['', Validators.required],
      other: this.formBuilder.array([]),
    });

    let storedInfo = localStorage.getItem('additional_transportation_questions');
    if (storedInfo) {
      this.transportationForm.patchValue(JSON.parse(storedInfo));
    }

    if (this.transportationForm.get('other_dist_checkbox').value == '1') {
      this.createExepense();
    }
  }

  newDonation(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        distance_description: [obj ? obj.distance_description : '',[Validators.required]],
        distance_value: [obj ? obj.distance_value : '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        distance_description: ['', Validators.required],
        distance_value: ['', Validators.required]
      });
    }

  }

  get other(): FormArray {
    return this.transportationForm.get('other') as FormArray;
  }

  addExpense(obj?) {
    // ;
    if (obj) {
      this.other.push(this.newDonation(obj));
    } else {
      this.other.push(this.newDonation());
    }

  }

  removeExpense(i: number) {
    this.other.removeAt(i);
  }

  createExepense = () => {
    let savedExpenses = this.getSavedVolunExpen();
    if (savedExpenses.length) {
      for (let x in savedExpenses) {
        this.addExpense(savedExpenses[x]);
      }
    } else {
      this.addExpense();
    }
  };

  getSavedVolunExpen() {
    let volunExpInformation = localStorage.getItem('additional_transportation_questions');
    volunExpInformation = JSON.parse(volunExpInformation);
    if (volunExpInformation) {

      return volunExpInformation['other'];
    }
    return [];
  }

  onSubmit() {
    if (!this.aiTaxOnlineService.validate(this.transportationForm)) {
      return false;
    }
    // stop here if form is invalid
    console.log(this.transportationForm.value);
    if (this.transportationForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('additional_transportation_questions', JSON.stringify(this.transportationForm.value));
      this.router.navigate(['aitax-online/entertainment-expenses']);
      this.aiTaxOnlineService.saveTransportation(this.transportationForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.success = 'Information saved successfully. We will verify your information and will activate your account in 1 day.';
          },
          error => {
            console.log(error);
            if (error.status == 422) {
              this.error = error.error.errors;
            }
            if (error.status == 500) {
              this.error = error.error.message;
            }
            if (error.status == 406) {
              this.error = error.error.message;
            }
            this.success = '';
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.transportationForm);
    }
  }

  back() {
    localStorage.setItem('additional_transportation_questions', JSON.stringify(this.transportationForm.value));
    this.router.navigate(['aitax-online/vehicle-insurance']);
  }

  toggleDependentsInfo(e) {
    if (e.value == '1') {
      this.createExepense();
    } else {
      this.clearFormArray(<FormArray>this.transportationForm.get('other'));
    }
  }

  clearFormArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }
  };
}
