import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';

import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";


@Component({
  selector: 'app-pocket-medical-expenses-2',
  templateUrl: './pocket-medical-expenses-2.component.html',
  styleUrls: ['./pocket-medical-expenses-2.component.css']
})
export class PocketMedicalExpenses2Component extends ActivityTimeTracker implements OnInit {

  section_name = 'Out of Pocket Medical Expenses';
  pocketMedicalExpensesForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  modalRef: NgbModalRef;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public aiTaxOnlineService: AITaxOnlineService,
    private progress: ProgressService,
    private modalService: NgbModal,
    private toastrService: ToastrService
  ) {
    super(aiTaxOnlineService)
    progress.changeProgress(31);
  }

  ngOnInit() {
    super.ngOnInit()
    this.pocketMedicalExpensesForm = this.formBuilder.group({
      out_of_pocket_medications_radio: ['', Validators.required],
      out_of_pocket_medications: [''],
      out_of_pocket_dental_radio: ['', Validators.required],
      out_of_pocket_dental: [''],
      out_of_pocket_other_radio: ['', Validators.required],
      out_of_pocket_other: ['']
    });

    let storedInfo = localStorage.getItem('pocket_medical_expenses_2');
    if (storedInfo) {
      this.pocketMedicalExpensesForm.patchValue(JSON.parse(storedInfo));
    }


  }

  onSubmit() {
    console.log(this.pocketMedicalExpensesForm.value);
    if (this.pocketMedicalExpensesForm.valid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('pocket_medical_expenses_2', JSON.stringify(this.pocketMedicalExpensesForm.value));

      this.aiTaxOnlineService.savePocketMedicalExpenses2Info(this.pocketMedicalExpensesForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Pocket Medical Expense Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/deposit-information']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      this.aiTaxOnlineService.touchAllFields(this.pocketMedicalExpensesForm);
    }
  }

  back() {
    localStorage.setItem('pocket_medical_expenses_2', JSON.stringify(this.pocketMedicalExpensesForm.value));
    this.router.navigate(['aitax-online/pocket-medical-expenses']);
  }

  openModal = (modelId): void => {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      size: 'md',
      windowClass: 'modal_extraDOc',
    };
    this.modalRef = this.modalService.open(modelId, ngbModalOptions);
  };
}
