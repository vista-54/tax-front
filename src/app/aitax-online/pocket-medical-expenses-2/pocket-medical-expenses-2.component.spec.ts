import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PocketMedicalExpenses2Component } from './pocket-medical-expenses-2.component';

describe('DepositInformationComponent', () => {
  let component: PocketMedicalExpenses2Component;
  let fixture: ComponentFixture<PocketMedicalExpenses2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PocketMedicalExpenses2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PocketMedicalExpenses2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
