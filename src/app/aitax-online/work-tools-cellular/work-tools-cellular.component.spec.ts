import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkToolsCellularComponent } from './work-tools-cellular.component';

describe('DepositInformationComponent', () => {
  let component: WorkToolsCellularComponent;
  let fixture: ComponentFixture<WorkToolsCellularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkToolsCellularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkToolsCellularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
