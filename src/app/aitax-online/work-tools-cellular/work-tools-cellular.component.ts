import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-work-tools-cellular',
  templateUrl: './work-tools-cellular.component.html',
  styleUrls: ['./work-tools-cellular.component.css']
})
export class WorkToolsCellularComponent extends ActivityTimeTracker implements OnInit {

  section_name = 'Work Tools & Misc';
  workToolCellularForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  modalRef: NgbModalRef;
  public isMarried: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private modalService: NgbModal,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(23);
  }

  ngOnInit() {
    super.ngOnInit()
    this.workToolCellularForm = this.formBuilder.group({
      ets_Monthly_Cell_Phone_Bill: ['', Validators.required],
      ets_Spouse_Monthly_Cell_Phone_Bill: [''],
      ets_Networking_Expenses_radio: ['', Validators.required],
      ets_Networking_Expenses: [''],
      ets_spouse_Networking_Expenses: [''],
      ets_education_expenses_description_checkbox: ['', Validators.required],
      certification_expenses: this.formBuilder.array([]),
      internet_pay: ['', Validators.required]
    });
    const personalInfo = localStorage.getItem('personal_information');
    const storedInfo = localStorage.getItem('work_tools_cellular');

    if (storedInfo) {
      this.workToolCellularForm.patchValue(JSON.parse(storedInfo));
    }
    if (personalInfo) {
      this.isMarried = JSON.parse(personalInfo).marital_status === '1';
    }

    if (this.workToolCellularForm.get('ets_education_expenses_description_checkbox').value == '1') {
      this.createExepense();
    }
  }

  newExpense(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: '',
        cost_value: ''
      });
    }

  }

  get expenses(): FormArray {
    return this.workToolCellularForm.get('certification_expenses') as FormArray;
  }

  addExpense(obj?) {
    // ;
    if (obj) {
      this.expenses.push(this.newExpense(obj));
    } else {
      this.expenses.push(this.newExpense());
    }

  }

  removeExpense(i: number) {
    this.expenses.removeAt(i);
  }

  createExepense = () => {
    let savedExpenses = this.getSavedExpen();
    if (savedExpenses.length) {
      for (let x in savedExpenses) {
        this.addExpense(savedExpenses[x]);
      }
    } else {
      this.addExpense();
    }
  };

  getSavedExpen() {
    let expInformation = localStorage.getItem('work_tools_cellular');
    expInformation = JSON.parse(expInformation);
    if (expInformation) {

      return expInformation['certification_expenses'];
    }
    return [];
  }

  toggleExpInfo(e) {
    console.log(e);
    if (e.value == '1') {
      this.createExepense();
    } else {
      this.clearExpArray(<FormArray>this.workToolCellularForm.get('certification_expenses'));
    }
  }

  clearExpArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };


  onSubmit() {
    if (!this.aiTaxOnlineService.validate(this.workToolCellularForm)) {
      return false;
    }
    // stop here if form is invalid
    console.log(this.workToolCellularForm.valid);
    if (this.workToolCellularForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('work_tools_cellular', JSON.stringify(this.workToolCellularForm.value));
      localStorage.setItem('internet_pay', JSON.stringify(this.workToolCellularForm.get('internet_pay').value));

      this.aiTaxOnlineService.saveWorkToolsCellular(this.workToolCellularForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Work Tools Cellular Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/work-tools-exam']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.workToolCellularForm);
    }
  }

  back() {
    localStorage.setItem('work_tools_cellular', JSON.stringify(this.workToolCellularForm.value));
    this.router.navigate(['aitax-online/work-tools']);
  }

  openModal = (modelId): void => {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      size: 'md',
      windowClass: 'modal_extraDOc',
    };
    this.modalRef = this.modalService.open(modelId, ngbModalOptions);
  };
}
