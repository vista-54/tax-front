import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-home-expenses',
  templateUrl: './home-expenses.component.html',
  styleUrls: ['./home-expenses.component.css']
})
export class HomeExpensesComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Home Expenses';
  homeExpForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  calculation:any;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(14);
  }

  ngOnInit() {
    super.ngOnInit();
    this.homeExpForm = this.formBuilder.group({
      hm_own_rent: ['', Validators.required],
      hm_mortage_interest: [''],
      hm_real_estate_tax_checkbox: [''],
      hm_real_estate_tax: [''],
      hm_mortgage_insurance: [''],
      hm_loan_mandated_homeowner_ins: [''],
      hm_energy_efficient_upgrades_radio: [''],
      // hm_energy_efficient_total_amount: [''],
      totals: this.formBuilder.array([]),
    });
    const storedInfo = localStorage.getItem('home_expenses');
    if (storedInfo) {
      let parsedInfo = JSON.parse(storedInfo);
      // if(parsedInfo.hm_own_rent == true){
      //   this.toggleValidation(parsedInfo.hm_own_rent);
      // }
      this.homeExpForm.patchValue(parsedInfo);
    }
    if (this.homeExpForm.get('hm_energy_efficient_upgrades_radio').value == '1') {
      this.createTotals();
      this.calc();
    }
  }

  get controls() {
    return this.homeExpForm.controls;
  }

  newTotals(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get totals(): FormArray {
    return this.homeExpForm.get('totals') as FormArray;
  }

  addTotals(obj?) {
    if (obj) {
      this.totals.push(this.newTotals(obj));
    } else {
      this.totals.push(this.newTotals());
    }

  }

  removeTotals(i: number) {
    this.totals.removeAt(i);
  }

  createTotals = () => {
    let savedTotals = this.getSavedTotals();
    if (savedTotals.length) {
      for (let x in savedTotals) {
        this.addTotals(savedTotals[x]);
      }
    } else {
      this.addTotals();
    }
  };

  getSavedTotals() {
    let savedTotals = localStorage.getItem('home_expenses');
    console.log(savedTotals);
    savedTotals = JSON.parse(savedTotals);
    if (savedTotals) {
      return savedTotals['totals'];
    }
    return [];
  }

  toggleTotals(e) {
    // console.log(e.target);
    if (e.value == '1') {
      this.createTotals();
    } else {
      this.clearUniformsArray(<FormArray>this.homeExpForm.get('totals'));
    }
  }

  clearUniformsArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  onSubmit() {


    // stop here if form is invalid
    if (!this.aiTaxOnlineService.validate(this.homeExpForm)) {
      return false;
    }
    console.log(this.homeExpForm.value);
    if (this.homeExpForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('home_expenses', JSON.stringify(this.homeExpForm.value));
      this.aiTaxOnlineService.saveHomeExpense(this.homeExpForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Home Expenses Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/small-items']);

            // this.router.navigate([this.returnUrl]);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.homeExpForm);
    }
  }

  back() {
    localStorage.setItem('home_expenses', JSON.stringify(this.homeExpForm.value));
    this.router.navigate(['aitax-online/adoption-expenses']);
  }

  toggleValidation(value) {
    let hm_mortage_interest = this.homeExpForm.get('hm_mortage_interest');
    let hm_real_estate_tax_checkbox = this.homeExpForm.get('hm_real_estate_tax_checkbox');
    let hm_real_estate_tax = this.homeExpForm.get('hm_real_estate_tax');
    let hm_mortgage_insurance = this.homeExpForm.get('hm_mortgage_insurance');
    let hm_loan_mandated_homeowner_ins = this.homeExpForm.get('hm_loan_mandated_homeowner_ins');
    let hm_energy_efficient_upgrades_radio = this.homeExpForm.get('hm_energy_efficient_upgrades_radio');


    if (value === 0) {
      hm_mortage_interest.setValidators(null);
      hm_real_estate_tax_checkbox.setValidators(null);
      hm_real_estate_tax.setValidators(null);
      hm_mortgage_insurance.setValidators(null);
      hm_loan_mandated_homeowner_ins.setValidators(null);
      hm_energy_efficient_upgrades_radio.setValidators(null);
    } else if (value === 1) {
      hm_mortage_interest.setValidators([Validators.required]);
      hm_real_estate_tax_checkbox.setValidators(null);
      hm_real_estate_tax.setValidators(null);
      hm_mortgage_insurance.setValidators(null);
      hm_loan_mandated_homeowner_ins.setValidators(null);
      hm_energy_efficient_upgrades_radio.setValidators(null);
    } else if (value === 2) {
      hm_mortage_interest.setValidators([Validators.required]);
      hm_real_estate_tax_checkbox.setValidators([Validators.required]);
      hm_real_estate_tax.setValidators(null);
      hm_mortgage_insurance.setValidators(null);
      hm_loan_mandated_homeowner_ins.setValidators(null);
      hm_energy_efficient_upgrades_radio.setValidators([Validators.required]);
    } else if (value === 3) {
      hm_mortage_interest.setValidators([Validators.required]);
      hm_real_estate_tax_checkbox.setValidators([Validators.required]);
      hm_real_estate_tax.setValidators([Validators.required]);
      hm_mortgage_insurance.setValidators([Validators.required]);
      hm_loan_mandated_homeowner_ins.setValidators([Validators.required]);
      hm_energy_efficient_upgrades_radio.setValidators([Validators.required]);
    }
  }

  calc() {
    let calc = 0;
    let arr = this.homeExpForm.get('totals').value;
    arr.map((item, index, array) => {
      calc += +item.cost_value;
    });
    this.calculation = calc;
  }

}
