import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first, mergeMap} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";
import {from} from "rxjs";

@Component({
  selector: 'app-tuition-related-expenses',
  templateUrl: './tuition-related-expenses.component.html',
  styleUrls: ['./tuition-related-expenses.component.css']
})
export class TuitionRelatedExpensesComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Education and Tuition-Related Expenses';
  tuitionExpForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService)
    progress.changeProgress(12);
  }

  ngOnInit() {
    super.ngOnInit();
    this.tuitionExpForm = this.formBuilder.group({
      se_enrolled_in_college_checkbox: ['', Validators.required],
      tuition_expenses: this.formBuilder.array([]),
      se_load_checkbox: ['', Validators.required],
      loan_expenses: this.formBuilder.array([]),
    });

    var storedInfo = localStorage.getItem('tuition_related_expenses');
    if (storedInfo) {
      this.tuitionExpForm.patchValue(JSON.parse(storedInfo));
    }

    if (this.tuitionExpForm.get('se_enrolled_in_college_checkbox').value == '1') {
      // ;
      this.createExepense();
    }

    const storedLoanInfo = localStorage.getItem('tuition_loan_expenses');
    if (storedLoanInfo) {
      this.tuitionExpForm.patchValue(JSON.parse(storedLoanInfo));
    }

    if (this.tuitionExpForm.get('se_load_checkbox').value == '1') {
      // ;
      this.createLoanExepense();
    }
  }

  newDonation(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get tutionExpenses(): FormArray {
    return this.tuitionExpForm.get('tuition_expenses') as FormArray;
  }

  get loanExpenses(): FormArray {
    return this.tuitionExpForm.get('loan_expenses') as FormArray;
  }

  addExpense(obj?) {
    // ;
    if (obj) {
      this.tutionExpenses.push(this.newDonation(obj));
    } else {
      this.tutionExpenses.push(this.newDonation());
    }

  }

  addLoanExpense(obj?) {
    // ;
    if (obj) {
      this.loanExpenses.push(this.newDonation(obj));
    } else {
      this.loanExpenses.push(this.newDonation());
    }

  }

  removeExpense(i: number) {
    this.tutionExpenses.removeAt(i);
  }

  createExepense = () => {
    var savedTuitionExp = this.getSavedVolunExpen();
    if (savedTuitionExp.length) {
      for (let x in savedTuitionExp) {
        this.addExpense(savedTuitionExp[x]);
      }
    } else {
      this.addExpense();
    }
  };

  createLoanExepense = () => {
    var savedTuitionExp = this.getSavedLoanExp();
    if (savedTuitionExp.length) {
      for (let x in savedTuitionExp) {
        this.addLoanExpense(savedTuitionExp[x]);
      }
    } else {
      this.addLoanExpense();
    }
  };

  getSavedVolunExpen() {
    let tuitionExpInfo = localStorage.getItem('tuition_related_expenses');
    tuitionExpInfo = JSON.parse(tuitionExpInfo);
    if (tuitionExpInfo) {

      return tuitionExpInfo['tuition_expenses'];
    }
    return [];
  }

  getSavedLoanExp() {
    let tuitionLoanExpInfo = localStorage.getItem('tuition_loan_expenses');
    tuitionLoanExpInfo = JSON.parse(tuitionLoanExpInfo);
    if (tuitionLoanExpInfo) {

      return tuitionLoanExpInfo['loan_expenses'];
    }
    return [];
  }

  onSubmit() {
    if (!this.aiTaxOnlineService.validate(this.tuitionExpForm)) {
      return false;
    }
    // stop here if form is invalid
    console.log(this.tuitionExpForm.value);
    if (this.tuitionExpForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      const tution_r_e = {
        request:'tution',
        se_enrolled_in_college_checkbox: this.tuitionExpForm.get('se_enrolled_in_college_checkbox').value,
        tuition_expenses: this.tuitionExpForm.get('tuition_expenses').value
      };
      const loan_r_e = {
        request:'loan',
        se_load_checkbox: this.tuitionExpForm.get('se_load_checkbox').value,
        loan_expenses: this.tuitionExpForm.get('loan_expenses').value
      };
      localStorage.setItem('tuition_related_expenses', JSON.stringify(tution_r_e));
      localStorage.setItem('tuition_loan_expenses', JSON.stringify(loan_r_e));
      from([tution_r_e,loan_r_e])
        .pipe(
          mergeMap(param=>this.sendToServer(param)),
          first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Tuition Related Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/adoption-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.toastrService.warning('Please, fill all fields. All fields are required', 'Warning!');
      this.aiTaxOnlineService.touchAllFields(this.tuitionExpForm);
    }
  }

  back() {
    localStorage.setItem('tuition_related_expenses', JSON.stringify(this.tuitionExpForm.value));
    this.router.navigate(['aitax-online/charitable-donations']);
  }

  toggleDependentsInfo(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createExepense();
    } else {
      this.clearFormArray(<FormArray>this.tuitionExpForm.get('tuition_expenses'));
    }
  }

  toggleLoanInfo(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createLoanExepense();
    } else {
      this.clearFormArray(<FormArray>this.tuitionExpForm.get('loan_expenses'));
    }
  }

  clearFormArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  sendToServer(obj) {
    switch (obj.request) {
      case 'tution':
        delete obj.request;
        return this.aiTaxOnlineService.saveTuitionRelExp(obj);
      case 'loan':
        delete obj.request;
        return this.aiTaxOnlineService.saveTuitionExpenses(obj);
    }
  }

}
