import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TuitionRelatedExpensesComponent } from './tuition-related-expenses.component';

describe('TuitionRelatedExpensesComponent', () => {
  let component: TuitionRelatedExpensesComponent;
  let fixture: ComponentFixture<TuitionRelatedExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TuitionRelatedExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TuitionRelatedExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
