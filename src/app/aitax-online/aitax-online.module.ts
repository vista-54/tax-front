import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IntroductionComponent} from './introduction/introduction.component';
import {PersonalInformationComponent} from './personal-information/personal-information.component';
import {SpouseInformationComponent} from './spouse-information/spouse-information.component';
import {AITaxOnlineRoutingModule} from './aitax-online.routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SpouseAdditionalInfoComponent} from './spouse-additional-info/spouse-additional-info.component';
import {CohanRuleComponent} from './cohan-rule/cohan-rule.component';
import {BasicInformationComponent} from './basic-information/basic-information.component';
import {DependentInformationComponent} from './dependent-information/dependent-information.component';
import {FbarInformationComponent} from './fbar-information/fbar-information.component';
import {RentalPropertyInformationComponent} from './rental-property-information/rental-property-information.component';
import {CharitableDonationsComponent} from './charitable-donations/charitable-donations.component';
import {PropertyDonationsComponent} from './property-donations/property-donations.component';
import {VolunteerExpensesComponent} from './volunteer-expenses/volunteer-expenses.component';
import {TuitionRelatedExpensesComponent} from './tuition-related-expenses/tuition-related-expenses.component';
import {TuitionLoanExpensesComponent} from './tuition-loan-expenses/tuition-loan-expenses.component';
import {AdoptionExpensesComponent} from './adoption-expenses/adoption-expenses.component';
import {HomeExpensesComponent} from './home-expenses/home-expenses.component';
import {SmallItemsComponent} from './small-items/small-items.component';
import {BusinessExpensesComponent} from './business-expenses/business-expenses.component';
import {VehicleExpensesComponent} from './vehicle-expenses/vehicle-expenses.component';
import {VehicleMaintenanceExpensesComponent} from './vehicle-maintenance-expenses/vehicle-maintenance-expenses.component';
import {VehicleInsuranceComponent} from './vehicle-insurance/vehicle-insurance.component';
import {EntertainmentExpensesComponent} from './entertainment-expenses/entertainment-expenses.component';
import {ThanksScreenComponent} from './thanks-screen/thanks-screen.component';
import {LocalStorageInfo} from '../helpers/localstorate';
import {WorkToolsComponent} from './work-tools/work-tools.component';
import {WorkToolsCellularComponent} from './work-tools-cellular/work-tools-cellular.component';
import {WorkToolsExamComponent} from './work-tools-exam/work-tools-exam.component';
import {WorkToolsResearchComponent} from './work-tools-research/work-tools-research.component';
import {EmergencyPersonalComponent} from './emergency-personal/emergency-personal.component';
import {BlueCollarExpenses2Component} from './blue-collar-expenses-2/blue-collar-expenses-2.component';
import {BlueCollarExpensesComponent} from './blue-collar-expenses/blue-collar-expenses.component';
import {EmergencyPersonalExpensesComponent} from './emergency-personal-expenses/emergency-personal-expenses.component';
import {PocketMedicalExpensesComponent} from './pocket-medical-expenses/pocket-medical-expenses.component';
import {PocketMedicalExpenses2Component} from './pocket-medical-expenses-2/pocket-medical-expenses-2.component';
import {ConclusionComponent} from './conclusion/conclusion.component';
import {SharedModule} from '../shared/shared.module';
import {AITaxErrorHanler} from '../helpers/error.handler';
import {AdditionalTransportationQuestionsComponent} from './additional-transportation-questions/additional-transportation-questions.component';
import {NgxMaskModule} from 'ngx-mask';
import {DepositInformationComponent} from './deposit-information/deposit-information.component';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatStepperModule} from '@angular/material/stepper';
import {SideIncomInformationComponent} from "./side-incom-information/side-incom-information.component";


@NgModule({
  declarations: [
    IntroductionComponent,
    DepositInformationComponent,
    PersonalInformationComponent,
    PocketMedicalExpensesComponent,
    PocketMedicalExpenses2Component,
    SpouseInformationComponent,
    SpouseAdditionalInfoComponent,
    CohanRuleComponent,
    AdditionalTransportationQuestionsComponent,
    BasicInformationComponent,
    DependentInformationComponent,
    FbarInformationComponent,
    EmergencyPersonalComponent,
    RentalPropertyInformationComponent,
    CharitableDonationsComponent,
    PropertyDonationsComponent,
    VolunteerExpensesComponent,
    TuitionRelatedExpensesComponent,
    TuitionLoanExpensesComponent,
    BlueCollarExpenses2Component,
    BlueCollarExpensesComponent,
    EmergencyPersonalExpensesComponent,
    AdoptionExpensesComponent,
    HomeExpensesComponent,
    SmallItemsComponent,
    BusinessExpensesComponent,
    VehicleExpensesComponent,
    VehicleMaintenanceExpensesComponent,
    VehicleInsuranceComponent,
    EntertainmentExpensesComponent,
    ThanksScreenComponent,
    WorkToolsComponent,
    WorkToolsCellularComponent,
    WorkToolsExamComponent,
    WorkToolsResearchComponent,
    ConclusionComponent,
    SideIncomInformationComponent],
  imports: [
    CommonModule,
    AITaxOnlineRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatInputModule,
    MatRadioModule,
    MatCheckboxModule,
    MatStepperModule,
    NgxMaskModule.forRoot({})
  ],
  providers: [LocalStorageInfo, AITaxErrorHanler]
})
export class AitaxOnlineModule {
}
