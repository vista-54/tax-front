import {Component, ElementRef, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {AITaxOnlineService} from "../aitax-online.service";

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.css']
})
export class IntroductionComponent {
  @ViewChild('video') video: ElementRef;
  public isVideoFinished: boolean = false;
  public didHasReportToUnlock: boolean;

  constructor(private formBuilder: FormBuilder,
              private router: Router, private aiTaxOnlineService: AITaxOnlineService) {
    let videoFinished = localStorage.getItem('introduction_video');
    this.didHasReportToUnlock = JSON.parse(localStorage.getItem('didHasReportToUnlock')) as boolean;
    if (videoFinished) {
      this.isVideoFinished = true;
    }
  }

  unlock() {
    console.log('unlock');
    this.aiTaxOnlineService.unlock()
      .subscribe((res) => {
        console.log(res);
        if (res['status'].hasOwnProperty('id')) {
          this.router.navigate(['aitax-online/personal-information']);
        }
      })
  }

  onEnd() {
    this.isVideoFinished = true;
    localStorage['introduction_video'] = true;
  }

  onSubmit() {
    this.router.navigate(['aitax-online/personal-information']);
  }

}
