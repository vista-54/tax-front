import { Component, OnInit } from '@angular/core';
import { AITaxOnlineService } from '../aitax-online.service';
import { first } from 'rxjs/operators';
import { LocalStorageInfo } from '../../helpers/localstorate';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {ProgressService} from '../../shared/services/progress.service';

@Component({
  selector: 'app-thanks-screen',
  templateUrl: './thanks-screen.component.html',
  styleUrls: ['./thanks-screen.component.css']
})
export class ThanksScreenComponent implements OnInit {

  loading = false;
  submitted = false;
  error = '';
  success = '';
  constructor(
    private aiTaxOnlineService: AITaxOnlineService,
    private router: Router,
    private localStorageInfo: LocalStorageInfo,
    private progress: ProgressService
  ) { }

  ngOnInit() {

  }

  onSave() {
    // this.loading = true;
    // let aitax_online = this.localStorageInfo.getAllInfo();
    // this.aiTaxOnlineService.saveCompleteAiTax(aitax_online)
    //     .pipe(first())
    //     .subscribe(
    //     data => {
    //       this.submitted = true;
    //       this.loading = false;
    //       this.success = 'Information updated successfylly.'
    //       // this.router.navigate([this.returnUrl]);
    //     },
    //     error => {
    //       console.log(error);
    //       if(error.status == 422){
    //           this.error = error.error.errors;
    //       }
    //       if(error.status == 500){
    //         this.error = error.error.message;
    //       }
    //       if(error.status == 406){
    //         this.error = error.error.message;
    //       }
    //       this.success = 'Information updated successfylly.'
    //       this.loading = false;
    //       this.submitted = false;
    //   });
  }

  back(){
    this.router.navigate(['aitax-online/etertainment-expenses'])
  }

}
