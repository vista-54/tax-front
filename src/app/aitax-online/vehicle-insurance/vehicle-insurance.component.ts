import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-vehicle-insurance',
  templateUrl: './vehicle-insurance.component.html',
  styleUrls: ['./vehicle-insurance.component.css']
})
export class VehicleInsuranceComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Vehicle Expenses'
  vehicleInsuranceForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  isMarried: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(19);
  }

  ngOnInit() {
    super.ngOnInit()
    this.vehicleInsuranceForm = this.formBuilder.group({
      eves_Total_Auto_Insurance: ['', Validators.required],
      eves_inspection_Total_Costs_radio: ['', Validators.required],
      eves_inspection_Total_Costs: [''],
      eves_spouse_inspection_Total_Costs: ['']
    });
    const storedInfo = localStorage.getItem('vehicle_insurance');
    const personalInfo = localStorage.getItem('personal_information');
    if (personalInfo) {
      this.isMarried = JSON.parse(personalInfo).marital_status === '1';
    }
    if (storedInfo) {
      this.vehicleInsuranceForm.patchValue(JSON.parse(storedInfo));
    }
  }

  onSubmit() {

    if (!this.aiTaxOnlineService.validate(this.vehicleInsuranceForm)) {
      return false;
    }
    // stop here if form is invalid
    console.log(this.vehicleInsuranceForm.value);
    if (this.vehicleInsuranceForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('vehicle_insurance', JSON.stringify(this.vehicleInsuranceForm.value));

      this.aiTaxOnlineService.saveVehicleInsurance(this.vehicleInsuranceForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Vehicle Insurance Expense Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/additional-transportation-questions']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.vehicleInsuranceForm);
    }
  }

  back() {
    localStorage.setItem('vehicle_insurance', JSON.stringify(this.vehicleInsuranceForm.value));
    this.router.navigate(['aitax-online/vehicle-maintenance-expenses']);
  }

}
