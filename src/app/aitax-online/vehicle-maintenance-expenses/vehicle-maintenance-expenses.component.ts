import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
    selector: 'app-vehicle-maintenance-expenses',
    templateUrl: './vehicle-maintenance-expenses.component.html',
    styleUrls: ['./vehicle-maintenance-expenses.component.css']
})
export class VehicleMaintenanceExpensesComponent extends ActivityTimeTracker implements OnInit {
    section_name = 'Vehicle Expenses';
    vehicleMaintExpForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';
    success = '';
    isMarried: boolean = false;

    constructor(private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                public aiTaxOnlineService: AITaxOnlineService,
                private progress: ProgressService,
                private toastrService: ToastrService) {
      super(aiTaxOnlineService);
      progress.changeProgress(18);
    }

    ngOnInit() {
      super.ngOnInit()
        this.vehicleMaintExpForm = this.formBuilder.group({
            eve_weekly_estimated_total: ['', Validators.required],
            eve_spouse_weekly_estimated_total: [''],
            eve_no_Oil_Changes: ['', Validators.required],
            eve_Cost_Per_Change: ['', [Validators.required]],
            eve_spouse_no_Oil_Changes: [''],
            eve_spouse_Cost_Per_Change: [''],
            eve_Total_Tire_Costs_radio: ['', Validators.required],
            eve_Total_Tire_Costs: [''],
            eve_spouse_Total_Tire_Costs: [''],
            eve_Total_Repairs_Cost_radio: ['', Validators.required],
            eve_Total_Repairs_Cost: [''],
            eve_spouse_Total_Repairs_Cost: [''],
        });

        const personalInfo = localStorage.getItem('personal_information');
        if (personalInfo) {
            this.isMarried = JSON.parse(personalInfo).marital_status === '1';
        }
        var storedInfo = localStorage.getItem('vehicle_maintanence_expenses');
        if (storedInfo) {
            this.vehicleMaintExpForm.patchValue(JSON.parse(storedInfo));
        }
    }

    onSubmit() {

      if (!this.aiTaxOnlineService.validate(this.vehicleMaintExpForm)) {
        return false;
      }
        // stop here if form is invalid
        console.log(this.vehicleMaintExpForm.value);
        if (this.vehicleMaintExpForm.valid) {
            // if (!this.registerForm.invalid) {
            this.submitted = true;
            this.loading = true;
            localStorage.setItem('vehicle_maintanence_expenses', JSON.stringify(this.vehicleMaintExpForm.value));

            this.aiTaxOnlineService.saveVehicleMaintenance(this.vehicleMaintExpForm.value)
                .pipe(first())
                .subscribe(
                    data => {
                        this.submitted = true;
                        this.loading = false;
                        this.toastrService.success('Vehicle Maintenance Expense Information saved successfully.', 'Success!');
                        this.router.navigate(['aitax-online/vehicle-insurance']);
                    },
                    errorRes => {
                        this.toastrService.error(errorRes.error.message, 'Error!');
                        this.loading = false;
                        this.submitted = false;
                    });
        } else {
            this.loading = false;
            this.submitted = false;
            this.error = '';
            // alert('invalid');
            this.aiTaxOnlineService.touchAllFields(this.vehicleMaintExpForm);
        }
    }

    back() {
      localStorage.setItem('vehicle_maintanence_expenses', JSON.stringify(this.vehicleMaintExpForm.value));
      this.router.navigate(['aitax-online/vehicle-expenses']);
    }

}
