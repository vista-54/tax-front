import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleMaintenanceExpensesComponent } from './vehicle-maintenance-expenses.component';

describe('VehicleMaintenanceExpensesComponent', () => {
  let component: VehicleMaintenanceExpensesComponent;
  let fixture: ComponentFixture<VehicleMaintenanceExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleMaintenanceExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleMaintenanceExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
