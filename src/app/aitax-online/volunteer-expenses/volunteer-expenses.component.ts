import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-volunteer-expenses',
  templateUrl: './volunteer-expenses.component.html',
  styleUrls: ['./volunteer-expenses.component.css']
})
export class VolunteerExpensesComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Volunteering Expenses';
  voluntExpForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public aiTaxOnlineService: AITaxOnlineService,
    private progress: ProgressService,
    private toastrService: ToastrService
  ) {
    super(aiTaxOnlineService)
    progress.changeProgress(13);
  }

  ngOnInit() {
    super.ngOnInit();
    this.voluntExpForm = this.formBuilder.group({
      ve_volunteering_checkbox: ['', Validators.required],
      expenses: this.formBuilder.array([]),
    });

    let storedInfo = localStorage.getItem('volunter_expenses');
    if (storedInfo) {
      this.voluntExpForm.patchValue(JSON.parse(storedInfo));
    }

    if (this.voluntExpForm.get('ve_volunteering_checkbox').value == '1') {
      // ;
      this.createExepense();
    }
  }

  newDonation(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get expenses(): FormArray {
    return this.voluntExpForm.get('expenses') as FormArray;
  }

  addExpense(obj?) {
    // ;
    if (obj) {
      this.expenses.push(this.newDonation(obj));
    } else {
      this.expenses.push(this.newDonation());
    }

  }

  removeExpense(i: number) {
    this.expenses.removeAt(i);
  }

  createExepense = () => {
    var savedExpenses = this.getSavedVolunExpen();
    if (savedExpenses.length) {
      for (let x in savedExpenses) {
        this.addExpense(savedExpenses[x]);
      }
    } else {
      this.addExpense();
    }
  };

  getSavedVolunExpen() {
    let volunExpInformation = localStorage.getItem('volunter_expenses');
    volunExpInformation = JSON.parse(volunExpInformation);
    if (volunExpInformation) {

      return volunExpInformation['expenses'];
    }
    return [];
  }

  onSubmit() {
    // stop here if form is invalid
    console.log(this.voluntExpForm.value);
    if (this.voluntExpForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('volunter_expenses', JSON.stringify(this.voluntExpForm.value));
      this.router.navigate(['aitax-online/tuition-related-expenses']);
      this.aiTaxOnlineService.saveVolExp(this.voluntExpForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Volunteer Expense Information saved successfully.', 'Success!');
            // this.router.navigate([this.returnUrl]);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.voluntExpForm);
    }
  }

  back() {
    localStorage.setItem('volunter_expenses', JSON.stringify(this.voluntExpForm.value));
    this.router.navigate(['aitax-online/property-donations']);
  }

  toggleDependentsInfo(e) {
    // console.log(e.target.value);
    if (e.target.value == '1') {
      this.createExepense();
    } else {
      this.clearFormArray(<FormArray>this.voluntExpForm.get('expenses'));
    }
  }

  clearFormArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

}
