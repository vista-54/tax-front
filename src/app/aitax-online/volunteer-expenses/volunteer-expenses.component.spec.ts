import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteerExpensesComponent } from './volunteer-expenses.component';

describe('VolunteerExpensesComponent', () => {
  let component: VolunteerExpensesComponent;
  let fixture: ComponentFixture<VolunteerExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolunteerExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteerExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
