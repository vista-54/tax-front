import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {IntroductionComponent} from './introduction/introduction.component';
import {PersonalInformationComponent} from './personal-information/personal-information.component';
import {SpouseInformationComponent} from './spouse-information/spouse-information.component';
import {SpouseAdditionalInfoComponent} from './spouse-additional-info/spouse-additional-info.component';
import {CohanRuleComponent} from './cohan-rule/cohan-rule.component';
import {BasicInformationComponent} from './basic-information/basic-information.component';
import {DependentInformationComponent} from './dependent-information/dependent-information.component';
import {FbarInformationComponent} from './fbar-information/fbar-information.component';
import {RentalPropertyInformationComponent} from './rental-property-information/rental-property-information.component';
import {CharitableDonationsComponent} from './charitable-donations/charitable-donations.component';
import {PropertyDonationsComponent} from './property-donations/property-donations.component';
import {VolunteerExpensesComponent} from './volunteer-expenses/volunteer-expenses.component';
import {TuitionRelatedExpensesComponent} from './tuition-related-expenses/tuition-related-expenses.component';
import {TuitionLoanExpensesComponent} from './tuition-loan-expenses/tuition-loan-expenses.component';
import {AdoptionExpensesComponent} from './adoption-expenses/adoption-expenses.component';
import {HomeExpensesComponent} from './home-expenses/home-expenses.component';
import {SmallItemsComponent} from './small-items/small-items.component';
import {BusinessExpensesComponent} from './business-expenses/business-expenses.component';
import {VehicleExpensesComponent} from './vehicle-expenses/vehicle-expenses.component';
import {VehicleMaintenanceExpensesComponent} from './vehicle-maintenance-expenses/vehicle-maintenance-expenses.component';
import {VehicleInsuranceComponent} from './vehicle-insurance/vehicle-insurance.component';
import {EntertainmentExpensesComponent} from './entertainment-expenses/entertainment-expenses.component';
import {ThanksScreenComponent} from './thanks-screen/thanks-screen.component';
import {WorkToolsComponent} from './work-tools/work-tools.component';
import {WorkToolsCellularComponent} from './work-tools-cellular/work-tools-cellular.component';
import {EmergencyPersonalComponent} from './emergency-personal/emergency-personal.component';
import {BlueCollarExpenses2Component} from './blue-collar-expenses-2/blue-collar-expenses-2.component';
import {BlueCollarExpensesComponent} from './blue-collar-expenses/blue-collar-expenses.component';
import {EmergencyPersonalExpensesComponent} from './emergency-personal-expenses/emergency-personal-expenses.component';
import {PocketMedicalExpensesComponent} from './pocket-medical-expenses/pocket-medical-expenses.component';
import {PocketMedicalExpenses2Component} from './pocket-medical-expenses-2/pocket-medical-expenses-2.component';
import {WorkToolsExamComponent} from './work-tools-exam/work-tools-exam.component';
import {WorkToolsResearchComponent} from './work-tools-research/work-tools-research.component';
import {ConclusionComponent} from './conclusion/conclusion.component';
import {AdditionalTransportationQuestionsComponent} from './additional-transportation-questions/additional-transportation-questions.component';
import {DepositInformationComponent} from './deposit-information/deposit-information.component';
import {SideIncomInformationComponent} from "./side-incom-information/side-incom-information.component";


const routes: Routes = [

  {
    path: '',
    component: IntroductionComponent
  },
  {
    path: 'additional-transportation-questions',
    component: AdditionalTransportationQuestionsComponent
  },
  {
    path: 'deposit-information',
    component: DepositInformationComponent
  },
  {
    path: 'personal-information',
    component: PersonalInformationComponent
  },
  {
    path: 'pocket-medical-expenses',
    component: PocketMedicalExpensesComponent
  },
  {
    path: 'pocket-medical-expenses-2',
    component: PocketMedicalExpenses2Component
  },
  {
    path: 'spouse-information',
    component: SpouseInformationComponent
  },
  {
    path: 'spouse-additional-info',
    component: SpouseAdditionalInfoComponent
  },
  {
    path: 'cohan-rule',
    component: CohanRuleComponent
  },
  {
    path: 'basic-information',
    component: BasicInformationComponent
  },
  {
    path: 'blue-collar-expenses',
    component: BlueCollarExpensesComponent
  },
  {
    path: 'blue-collar-expenses-2',
    component: BlueCollarExpenses2Component
  },
  {
    path: 'dependent-information',
    component: DependentInformationComponent
  },
  {
    path: 'side-income-information',
    component: SideIncomInformationComponent
  },
  {
    path: 'emergency-personal',
    component: EmergencyPersonalComponent
  },
  {
    path: 'emergency-personal-expenses',
    component: EmergencyPersonalExpensesComponent
  },
  {
    path: 'fbar-information',
    component: FbarInformationComponent
  },
  {
    path: 'rental-property-information',
    component: RentalPropertyInformationComponent
  },

  {
    path: 'charitable-donations',
    component: CharitableDonationsComponent
  },
  {
    path: 'property-donations',
    component: PropertyDonationsComponent
  },
  {
    path: 'volunter-expenses',
    component: VolunteerExpensesComponent
  },
  {
    path: 'tuition-related-expenses',
    component: TuitionRelatedExpensesComponent
  },
  {
    path: 'tuition-loan-expenses',
    component: TuitionLoanExpensesComponent
  },
  {
    path: 'adoption-expenses',
    component: AdoptionExpensesComponent
  },
  {
    path: 'home-expenses',
    component: HomeExpensesComponent
  },
  {
    path: 'small-items',
    component: SmallItemsComponent
  },
  {
    path: 'business-expenses',
    component: BusinessExpensesComponent
  },
  {
    path: 'vehicle-expenses',
    component: VehicleExpensesComponent
  },
  {
    path: 'vehicle-maintenance-expenses',
    component: VehicleMaintenanceExpensesComponent
  },
  {
    path: 'vehicle-insurance',
    component: VehicleInsuranceComponent
  },
  {
    path: 'entertainment-expenses',
    component: EntertainmentExpensesComponent
  },
  {
    path: 'thanks',
    component: ThanksScreenComponent
  },
  {
    path: 'work-tools',
    component: WorkToolsComponent
  },
  {
    path: 'work-tools-exam',
    component: WorkToolsExamComponent
  },
  {
    path: 'work-tools-cellular',
    component: WorkToolsCellularComponent
  },
  {
    path: 'work-tools-research',
    component: WorkToolsResearchComponent
  },
  {
    path: 'conclusion',
    component: ConclusionComponent
  }
];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AITaxOnlineRoutingModule {
}
