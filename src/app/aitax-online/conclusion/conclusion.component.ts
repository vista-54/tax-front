import {Component, ElementRef, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AITaxOnlineService} from '../aitax-online.service';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../services/auth.service';

declare const document;

@Component({
  selector: 'app-introduction',
  templateUrl: './conclusion.component.html',
  styleUrls: ['./conclusion.component.css']
})
export class ConclusionComponent {
  @ViewChild('video') video: ElementRef;
  public isVideoFinished: boolean = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    progress.changeProgress(29);
  }


  onEnd() {
    this.isVideoFinished = true;
  }

  onSubmit() {
    this.aiTaxOnlineService.conclusion()
      .pipe(first())
      .subscribe(
        data => {
          this.toastrService.success('Congratulations on completing your 2019 tax interview. In the coming days we will be reviewing the interview and be reaching out with a tax proposal.', 'Success!');
          let toastr = document.getElementById('toast-container');
          console.log(toastr);
          toastr.classList.remove('toast-top-right');
          toastr.classList.add('toast-center-center');
          setTimeout(() => {
            this.authService.logout()
            this.router.navigate(['/login']);
            toastr.classList.remove('toast-center-center');
            toastr.classList.add('toast-top-right');
          }, 5000);
        },
        errorRes => {
          this.toastrService.error(errorRes.error.message, 'Error!');

        });

  }

  back() {
    this.router.navigate(['aitax-online/deposit-information']);
  }

}
