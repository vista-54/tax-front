import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
    selector: 'app-vehicle-expenses',
    templateUrl: './vehicle-expenses.component.html',
    styleUrls: ['./vehicle-expenses.component.css']
})
export class VehicleExpensesComponent extends ActivityTimeTracker  implements OnInit {
    section_name = 'Vehicle Expenses';
    public isMarried: boolean = false;
    vehicleExpForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';
    success = '';


    constructor(private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                public aiTaxOnlineService: AITaxOnlineService,
                private progress: ProgressService,
                private toastrService: ToastrService) {
      super(aiTaxOnlineService);
      progress.changeProgress(17);
    }

    ngOnInit() {
      super.ngOnInit();
        this.vehicleExpForm = this.formBuilder.group({
            ve_make: ['', Validators.required],
            ve_model: ['', Validators.required],
            ve_price: ['', Validators.required],
            own_lease: ['', Validators.required],
            spouse_own_lease: [''],
            ve_spouse_make: [''],
            ve_spouse_model: [''],
            ve_spouse_price: ['']
        });
        const storedInfo = localStorage.getItem('vehicle_expenses');
        const personalInfo = localStorage.getItem('personal_information');
        if (storedInfo) {
            this.vehicleExpForm.patchValue(JSON.parse(storedInfo));
        }
        if (personalInfo) {
            this.isMarried = JSON.parse(personalInfo).marital_status === '1';
        }
    }

    onSubmit() {

      if (!this.aiTaxOnlineService.validate(this.vehicleExpForm)) {
        return false;
      }
        // stop here if form is invalid
        console.log(this.vehicleExpForm.value);
        if (this.vehicleExpForm.valid) {
            // if (!this.registerForm.invalid) {
            this.submitted = true;
            this.loading = true;
            localStorage.setItem('vehicle_expenses', JSON.stringify(this.vehicleExpForm.value));

            this.aiTaxOnlineService.saveVehicleExp(this.vehicleExpForm.value)
                .pipe(first())
                .subscribe(
                    data => {
                        this.submitted = true;
                        this.loading = false;
                        this.toastrService.success('Vehicle Expenses Information saved successfully.', 'Success!');
                        this.router.navigate(['aitax-online/vehicle-maintenance-expenses']);
                    },
                    errorRes => {
                        this.toastrService.error(errorRes.error.message, 'Error!');
                        this.loading = false;
                        this.submitted = false;
                    });
        } else {
            this.loading = false;
            this.submitted = false;
            this.error = '';
            // alert('invalid');
            this.aiTaxOnlineService.touchAllFields(this.vehicleExpForm);
        }
    }

    back() {
      localStorage.setItem('vehicle_expenses', JSON.stringify(this.vehicleExpForm.value));
      this.router.navigate(['aitax-online/business-expenses']);
    }

}
