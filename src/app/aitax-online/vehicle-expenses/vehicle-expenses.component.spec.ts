import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleExpensesComponent } from './vehicle-expenses.component';

describe('VehicleExpensesComponent', () => {
  let component: VehicleExpensesComponent;
  let fixture: ComponentFixture<VehicleExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
