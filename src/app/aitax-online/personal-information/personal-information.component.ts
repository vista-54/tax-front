import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {USA_STATES} from '../../shared/constants/usa.states';
import {COUNTRIES} from '../../shared/constants/countries';
import {ProgressService} from '../../shared/services/progress.service';
import {AITaxErrorHanler} from '../../helpers/error.handler';
import {ToastrService} from 'ngx-toastr';
import {phoneCodes} from '../../shared/constants/phone-codes';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css']
})
export class PersonalInformationComponent extends ActivityTimeTracker implements OnInit, OnDestroy {
  section_name = 'Personal Information';
  personalInfo: FormGroup;
  public states: string[] = [];
  public defaultCountry: string = 'USA';
  public otherCitizen: string[] = [];
  public usaStates: string[] = [];
  public user: any;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  address: any;
  date_now: any;
  placeholder: string = 'Street, city, state, country';
  days = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  year = [];
  phoneCods = phoneCodes;


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService,
              private aiTaxErrorHanlder: AITaxErrorHanler) {
    super(aiTaxOnlineService);
    progress.changeProgress(2);
  }

  ngOnInit() {
    super.ngOnInit();
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    this.personalInfo = this.formBuilder.group({
      first_name: ['', Validators.required],
      middle_name: [''],
      last_name: ['', Validators.required],
      address: ['', Validators.required],
      zip: ['', Validators.required],
      social_security: ['', [Validators.required, Validators.maxLength(9)]],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]],
      // country_code: ['', Validators.required],
      marital_status: ['', Validators.required],
      citizenship_status: ['', Validators.required],
      visa_type: [''],
      citizenship_other: [''],
      job_title: ['', [Validators.required, Validators.maxLength(20)]],
      date_of_birth: ['', Validators.required],
      active_military: ['', Validators.required],
      active_military_state: [''],
      day: ['',Validators.required],
      month: ['',Validators.required],
      year: ['',Validators.required],
    });
    this.personalInfo.patchValue(this.user);
    const storedInfo = localStorage.getItem('personal_information');
    const address = localStorage.getItem('address');
    if (storedInfo) {
      const storedInformation = JSON.parse(storedInfo);
      this.personalInfo.patchValue(storedInformation);
    }
    if (address) {
      this.placeholder = address;
      this.personalInfo.patchValue({address})
    }
    console.log(this.personalInfo.get('date_of_birth').value);
    for (let i = new Date().getFullYear(); i >= 1930; i--) {
      this.year.push(i);
    }

    this.date_now = +new Date() * 1;


    this.otherCitizen = COUNTRIES;
    this.usaStates = USA_STATES;
  }


  async getAddress(place: object) {
    console.log(place);
    this.address = place;
    if (place['formatted_address']) {
      this.personalInfo.patchValue({address: place['formatted_address']});
    } else {
      this.personalInfo.patchValue({address: place});
    }
  }

  onSubmit() {
    console.log(this.personalInfo.value['address']);


    let err = 0;
    let arr = this.personalInfo.get('date_of_birth');
    console.log(arr.value);
    if ((+new Date(arr.value)) * 1 > this.date_now || arr.value == '') {
      err++;
    }

    if (err) {
      this.toastrService.warning('Please enter a valid date!', 'Warning!');
    } else {
      // stop here if form is invalid
      this.aiTaxOnlineService.validate(this.personalInfo)
      console.log(this.personalInfo.valid);
      if (this.personalInfo.valid) {
        // if (!this.registerForm.invalid) {
        this.submitted = true;
        this.loading = true;

        localStorage.setItem('personal_information', JSON.stringify(this.personalInfo.value));
        console.log(this.address);
        if (this.address) {
          if (this.address.hasOwnProperty('formatted_address')) {
            localStorage['address'] = this.address['formatted_address'];
          } else {
            localStorage['address'] = this.address;
          }
        }

        this.aiTaxOnlineService.savePersonalInfo(this.personalInfo.value)
          .pipe(first())
          .subscribe(
            data => {
              this.submitted = true;
              this.loading = false;
              this.toastrService.success('Personal Information saved successfully.', 'Success!');

              if (this.personalInfo.get('marital_status').value === '1') {
                this.router.navigate(['aitax-online/spouse-information']);
              } else {
                this.router.navigate(['aitax-online/spouse-additional-info']);
              }
            },
            errorRes => {
              // const message = this.aiTaxErrorHanlder.handleError(error.status, error.error);
              this.toastrService.error(errorRes.error.message, 'Error!');
              this.loading = false;
              this.submitted = false;
            });
      } else {
        this.loading = false;
        this.submitted = false;
        this.error = '';
        // alert('invalid');
        this.aiTaxOnlineService.touchAllFields(this.personalInfo);
      }
    }
  }

  back() {
    localStorage.setItem('personal_information', JSON.stringify(this.personalInfo.value));
    this.router.navigate(['aitax-online/']);
  }

  test() {
    let _day = this.personalInfo.get('day').value;
    let _month = this.personalInfo.get('month').value;
    let _year = this.personalInfo.get('year').value;


    if (_day !== '' && _month !== '' && _year !== '') {
      this.personalInfo.patchValue({date_of_birth: '' + _year + '-' + _month + '-' + _day});
    }
  }
}
