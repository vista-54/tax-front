import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-business-expenses',
  templateUrl: './business-expenses.component.html',
  styleUrls: ['./business-expenses.component.css']
})
export class BusinessExpensesComponent extends ActivityTimeTracker  implements OnInit {
  section_name = 'Employee Business Expenses';
  businessExpForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(16);
  }

  ngOnInit() {
    super.ngOnInit();
    this.businessExpForm = this.formBuilder.group({
      ebe_total_parking_fees_radio: ['', Validators.required],
      ebe_total_parking_fees: ['', Validators.required],
      ebe_tollway_costs_radio: ['', Validators.required],
      ebe_tollway_costs: ['', Validators.required],
      ebe_total_local_transportation_radio: ['', Validators.required],
      ebe_total_local_transportation: this.formBuilder.array([]),
      ebe_total_travel_costs_radio: ['', Validators.required],
      ebe_total_travel_costs: this.formBuilder.array([])
    });
    var storedInfo = localStorage.getItem('business_expenses');
    if (storedInfo) {
      this.businessExpForm.patchValue(JSON.parse(storedInfo));
    }

    if (this.businessExpForm.get('ebe_total_local_transportation_radio').value == '1') {
      this.createLocalTransportation();
    }
    if (this.businessExpForm.get('ebe_total_travel_costs_radio').value == '1') {
      this.createTravel();
    }
  }


  newLocalTransportation(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get localTransportation(): FormArray {
    return this.businessExpForm.get('ebe_total_local_transportation') as FormArray;
  }

  addLocalTransportation(obj?) {
    if (obj) {
      this.localTransportation.push(this.newLocalTransportation(obj));
    } else {
      this.localTransportation.push(this.newLocalTransportation());
    }

  }

  removeLocalTransportation(i: number) {
    this.localTransportation.removeAt(i);
  }

  createLocalTransportation = () => {
    let savedLocalTransportation = this.getSavedLocalTransportation();
    if (savedLocalTransportation.length) {
      for (let x in savedLocalTransportation) {
        this.addLocalTransportation(savedLocalTransportation[x]);
      }
    } else {
      this.addLocalTransportation();
    }
  };

  getSavedLocalTransportation() {
    let savedLocalTransportation = localStorage.getItem('business_expenses');
    console.log(savedLocalTransportation);
    savedLocalTransportation = JSON.parse(savedLocalTransportation);
    if (savedLocalTransportation) {
      return savedLocalTransportation['ebe_total_local_transportation'];
    }
    return [];
  }

  toggleLocalTransportation(e) {
    console.log(e.value);
    if (e.value == '1') {
      this.createLocalTransportation();
    } else {
      this.clearArray(<FormArray>this.businessExpForm.get('ebe_total_local_transportation'));
    }
  }

  //
  //
  //

  newTravel(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get travel(): FormArray {
    return this.businessExpForm.get('ebe_total_travel_costs') as FormArray;
  }

  addTravel(obj?) {
    if (obj) {
      this.travel.push(this.newTravel(obj));
    } else {
      this.travel.push(this.newTravel());
    }

  }

  removeTravel(i: number) {
    this.travel.removeAt(i);
  }

  createTravel = () => {
    let savedTravel = this.getSavedTravel();
    if (savedTravel.length) {
      for (let x in savedTravel) {
        this.addTravel(savedTravel[x]);
      }
    } else {
      this.addTravel();
    }
  };

  getSavedTravel() {
    let savedTravel = localStorage.getItem('business_expenses');
    console.log(savedTravel);
    savedTravel = JSON.parse(savedTravel);
    if (savedTravel) {
      return savedTravel['ebe_total_travel_costs'];
    }
    return [];
  }

  toggleTravel(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createTravel();
    } else {
      this.clearArray(<FormArray>this.businessExpForm.get('ebe_total_travel_costs'));
    }
  }

  clearArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  onSubmit() {
    // stop here if form is invalid
    if (!this.aiTaxOnlineService.validate(this.businessExpForm)) {
      return false;
    }
    console.log(this.businessExpForm.value);
    if (this.businessExpForm.get('ebe_total_parking_fees_radio').value == '0') {
      this.businessExpForm.removeControl('ebe_total_parking_fees');
    }
    if (this.businessExpForm.get('ebe_tollway_costs_radio').value == '0') {
      this.businessExpForm.removeControl('ebe_tollway_costs');
    }
    if (this.businessExpForm.get('ebe_total_local_transportation_radio').value == '0') {
      this.businessExpForm.removeControl('ebe_total_local_transportation');
    }
    if (this.businessExpForm.get('ebe_total_travel_costs_radio').value == '0') {
      this.businessExpForm.removeControl('ebe_total_travel_costs');
    }
    if (this.businessExpForm.valid) {
      this.submitted = true;
      this.loading = true;

      localStorage.setItem('business_expenses', JSON.stringify(this.businessExpForm.value));

      this.aiTaxOnlineService.saveBusinessExp(this.businessExpForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Business Expenses Information saved successfully', 'Success!');
            this.router.navigate(['aitax-online/vehicle-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.businessExpForm);
    }
  }

  back() {
    localStorage.setItem('business_expenses', JSON.stringify(this.businessExpForm.value));
    this.router.navigate(['aitax-online/small-items']);
  }

}
