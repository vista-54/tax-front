import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-adoption-expenses',
  templateUrl: './adoption-expenses.component.html',
  styleUrls: ['./adoption-expenses.component.css']
})
export class AdoptionExpensesComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Dependent Care Expenses & Adoption-Related Costs';
  adoptionExpForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService
  ) {
    super(aiTaxOnlineService)
    progress.changeProgress(13);
  }

  ngOnInit() {
    super.ngOnInit();
    this.adoptionExpForm = this.formBuilder.group({
      fm_add_description_checkbox: ['', Validators.required],
      fm_add_adoptions_checkbox: ['', Validators.required],
      // fm_adoption_cost_checkbox: ['', Validators.required],
      day_care_expenses: this.formBuilder.array([]),
      add_adoptions_expenses: this.formBuilder.array([]),
      // adoption_expenses: this.formBuilder.array([]),
    });

    var storedInfo = localStorage.getItem('all_adoption_expenses');
    if (storedInfo) {
      this.adoptionExpForm.patchValue(JSON.parse(storedInfo));
    }

    if (this.adoptionExpForm.get('fm_add_description_checkbox').value == '1') {
      // ;
      this.createDayCareExepense();
    }
    if (this.adoptionExpForm.get('fm_add_adoptions_checkbox').value == '1') {
      // ;
      this.createAddAdoptionsExp();
    }
    // if (this.adoptionExpForm.get('fm_adoption_cost_checkbox').value == '1') {
    //   // ;
    //   // this.createAdoptionExepense();
    // }
  }


  /***** Day Care Expense */
  newDayCareExp(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get dayCareExpenses(): FormArray {
    return this.adoptionExpForm.get('day_care_expenses') as FormArray;
  }

  addDayCareExp(obj?) {
    // ;
    if (obj) {
      this.dayCareExpenses.push(this.newDayCareExp(obj));
    } else {
      this.dayCareExpenses.push(this.newDayCareExp());
    }

  }

  removeDayCareExpense(i: number) {
    this.dayCareExpenses.removeAt(i);
  }

  createDayCareExepense = () => {
    var savedTuitionExp = this.getSavedDayCareExpen();
    if (savedTuitionExp.length) {
      for (let x in savedTuitionExp) {
        this.addDayCareExp(savedTuitionExp[x]);
      }
    } else {
      this.addDayCareExp();
    }
  };

  getSavedDayCareExpen() {
    let tuitionExpInfo = localStorage.getItem('all_adoption_expenses');
    tuitionExpInfo = JSON.parse(tuitionExpInfo);
    if (tuitionExpInfo) {

      return tuitionExpInfo['day_care_expenses'];
    }
    return [];
  }


  /***** Add Adoptions Expense */
  newAddAdoptionsExp(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get addAdoptionsExpenses(): FormArray {
    return this.adoptionExpForm.get('add_adoptions_expenses') as FormArray;
  }

  addAddAdoptionsExp(obj?) {
    // ;
    if (obj) {
      this.addAdoptionsExpenses.push(this.newAddAdoptionsExp(obj));
    } else {
      this.addAdoptionsExpenses.push(this.newAddAdoptionsExp());
    }

  }

  removeAddAdoptionsExp(i: number) {
    this.addAdoptionsExpenses.removeAt(i);
  }

  createAddAdoptionsExp = () => {
    let savedExp = this.getSavedAddAdoptionsExp();
    if (savedExp.length) {
      for (let x in savedExp) {
        this.addAddAdoptionsExp(savedExp[x]);
      }
    } else {
      this.addAddAdoptionsExp();
    }
  };

  getSavedAddAdoptionsExp() {
    let tuitionExpInfo = localStorage.getItem('all_adoption_expenses');
    tuitionExpInfo = JSON.parse(tuitionExpInfo);
    if (tuitionExpInfo) {
      return tuitionExpInfo['add_adoptions_expenses'];
    }
    return [];
  }

  toggleAddAdoptionsExp(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createAddAdoptionsExp();
    } else {
      this.clearFormArray(<FormArray>this.adoptionExpForm.get('add_adoptions_expenses'));
    }
  }


  onSubmit() {
    console.log(this.adoptionExpForm.value);
    // stop here if form is invalid
    if (!this.aiTaxOnlineService.validate(this.adoptionExpForm)) {
      return false;
    }
    if (this.adoptionExpForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('all_adoption_expenses', JSON.stringify(this.adoptionExpForm.value));
      this.router.navigate(['aitax-online/home-expenses']);
      this.aiTaxOnlineService.saveAddOptionExp(this.adoptionExpForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Expenses information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/home-expenses']);
          },
          errorRes => {

            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.adoptionExpForm);
    }
  }

  back() {
    localStorage.setItem('all_adoption_expenses', JSON.stringify(this.adoptionExpForm.value));
    this.router.navigate(['aitax-online/tuition-related-expenses']);
  }

  toggleDayCareInfo(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createDayCareExepense();
    } else {
      this.clearFormArray(<FormArray>this.adoptionExpForm.get('day_care_expenses'));
    }
  }


  clearFormArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };


  // // /** Adoption Expense */
  // get adoptionExpenses(): FormArray {
  //   return this.adoptionExpForm.get('adoption_expenses') as FormArray;
  // }
  //
  // newAdoptionExpense(obj?): FormGroup {
  //   if (obj) {
  //     return this.formBuilder.group({
  //       cost_description: [obj ? obj.cost_description : '',[Validators.required]],
  //        cost_value: [obj ? obj.cost_value: '',[Validators.required]]
  //     });
  //   } else {
  //     return this.formBuilder.group({
  //       cost_description: ['', Validators.required],
  //       cost_value: ['', Validators.required]
  //     });
  //   }
  //
  // }
  //
  // addAdoptionExpense(obj?) {
  //   // ;
  //   if (obj) {
  //     this.adoptionExpenses.push(this.newAdoptionExpense(obj));
  //   } else {
  //     this.adoptionExpenses.push(this.newAdoptionExpense());
  //   }
  //
  // }
  //
  // removeAdoptionExpense(i: number) {
  //   this.adoptionExpenses.removeAt(i);
  // }
  //
  // createAdoptionExepense = () => {
  //   var savedTuitionExp = this.getSavedAdopExpen();
  //   if (savedTuitionExp.length) {
  //     for (let x in savedTuitionExp) {
  //       this.addAdoptionExpense(savedTuitionExp[x]);
  //     }
  //   } else {
  //     this.addAdoptionExpense();
  //   }
  // };
  //
  // getSavedAdopExpen() {
  //   let tuitionExpInfo = localStorage.getItem('all_adoption_expenses');
  //   tuitionExpInfo = JSON.parse(tuitionExpInfo);
  //   if (tuitionExpInfo) {
  //
  //     return tuitionExpInfo['adoption_expenses'];
  //   }
  //   return [];
  // }
  // toggleAdoptionInfo(e) {
  //   // console.log(e.target.value);
  //   if (e.target.value == '1') {
  //     this.createAdoptionExepense();
  //   } else {
  //     this.clearFormArray(<FormArray>this.adoptionExpForm.get('adoption_expenses'));
  //   }
  // }

}
