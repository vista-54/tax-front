import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionExpensesComponent } from './adoption-expenses.component';

describe('AdoptionExpensesComponent', () => {
  let component: AdoptionExpensesComponent;
  let fixture: ComponentFixture<AdoptionExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
