import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-side-incom-information',
  templateUrl: './side-incom-information.component.html',
  styleUrls: []
})
export class SideIncomInformationComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Side Income Generating Activities'
  sideIncomeForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public aiTaxOnlineService: AITaxOnlineService,
    private progress: ProgressService,
    private toastrService: ToastrService
  ) {
    super(aiTaxOnlineService);
    progress.changeProgress(10);
  }

  ngOnInit() {
    super.ngOnInit();
    this.sideIncomeForm = this.formBuilder.group({
      income_expense: ['', Validators.required]
    });

    var storedInfo = localStorage.getItem('side_income');
    if (storedInfo) {
      this.sideIncomeForm.patchValue(JSON.parse(storedInfo));
    }
  }

  onSubmit() {

    if (!this.aiTaxOnlineService.validate(this.sideIncomeForm)) {
      return false;
    }
    // stop here if form is invalid
    if (this.sideIncomeForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('side_income', JSON.stringify(this.sideIncomeForm.value));

      this.aiTaxOnlineService.saveGA(this.sideIncomeForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Side Income Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/charitable-donations']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.sideIncomeForm);
    }
  }


  back() {
    localStorage.setItem('side_income', JSON.stringify(this.sideIncomeForm.value));
    this.router.navigate(['aitax-online/rental-property-information']);
  }

}
