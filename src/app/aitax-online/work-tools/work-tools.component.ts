import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
    selector: 'app-work-tools',
    templateUrl: './work-tools.component.html',
    styleUrls: ['./work-tools.component.css']
})
export class WorkToolsComponent  extends ActivityTimeTracker implements OnInit {
    section_name = 'Work Tools & Misc'
    workToolForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';
    success = '';
    modalRef: NgbModalRef;

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                public aiTaxOnlineService: AITaxOnlineService,
                private modalService: NgbModal,
                private progress: ProgressService,
                private toastrService: ToastrService) {
      super(aiTaxOnlineService)
        progress.changeProgress(22);
    }

    ngOnInit() {
      super.ngOnInit()
        this.workToolForm = this.formBuilder.group({
            et_office_furniture_description_radio: ['', Validators.required],
            office_furniture: this.formBuilder.array([]),
            et_office_equipment_description_radio: ['', Validators.required],
            office_equipment: this.formBuilder.array([]),
            et_seeking_expense_description_radio: ['', Validators.required],
            seeking_expense: this.formBuilder.array([]),
        });

        const storedInfo = localStorage.getItem('work_tools');
        if (storedInfo) {
            this.workToolForm.patchValue(JSON.parse(storedInfo));
        }

        if (this.workToolForm.get('et_office_furniture_description_radio').value === '1') {
            // ;
            this.createFurniture();
        }
        if (this.workToolForm.get('et_office_equipment_description_radio').value === '1') {
            // ;
            this.createEquipment();
        }
        if (this.workToolForm.get('et_seeking_expense_description_radio').value === '1') {
            // ;
            this.createExepense();
        }
    }

    newExpense(obj?): FormGroup {
        if (obj) {
            return this.formBuilder.group({
                cost_description: [obj ? obj.cost_description : '',[Validators.required]],
                 cost_value: [obj ? obj.cost_value: '',[Validators.required]]
            });
        } else {
            return this.formBuilder.group({
                cost_description: '',
                cost_value: ''
            });
        }

    }

    get expenses(): FormArray {
        return this.workToolForm.get('seeking_expense') as FormArray;
    }

    addExpense(obj?) {
        // ;
        if (obj) {
            this.expenses.push(this.newExpense(obj));
        } else {
            this.expenses.push(this.newExpense());
        }

    }

    removeExpense(i: number) {
        this.expenses.removeAt(i);
    }

    createExepense = () => {
        var savedExpenses = this.getSavedExpen();
        if (savedExpenses.length) {
            for (let x in savedExpenses) {
                this.addExpense(savedExpenses[x]);
            }
        } else {
            this.addExpense();
        }
    };

    getSavedExpen() {
        let expInformation = localStorage.getItem('work_tools');
        expInformation = JSON.parse(expInformation);
        if (expInformation) {

            return expInformation['seeking_expense'];
        }
        return [];
    }

    toggleExpenseInfo(e) {
        // console.log(e.target.value);
        if (e.value == '1') {
            this.createExepense();
        } else {
            this.clearExpArray(<FormArray>this.workToolForm.get('seeking_expense'));
        }
    }

    clearExpArray = (formArray: FormArray) => {
        if (formArray && formArray.length) {
            while (formArray.length !== 0) {
                formArray.removeAt(0);
            }
        }

    };

    /** Equibment */
    newEquipment(obj?): FormGroup {
        if (obj) {
            return this.formBuilder.group({
                cost_description: [obj ? obj.cost_description : '',[Validators.required]],
                 cost_value: [obj ? obj.cost_value: '',[Validators.required]]
            });
        } else {
            return this.formBuilder.group({
                cost_description: '',
                cost_value: ''
            });
        }

    }

    get equipment(): FormArray {
        return this.workToolForm.get('office_equipment') as FormArray;
    }

    addEquipment(obj?) {
        // ;
        if (obj) {
            this.equipment.push(this.newEquipment(obj));
        } else {
            this.equipment.push(this.newEquipment());
        }

    }

    removeEquipment(i: number) {
        this.equipment.removeAt(i);
    }

    createEquipment = () => {
        let savedFurniture = this.getSavedEquipment();
        if (savedFurniture.length) {
            for (let x in savedFurniture) {
                this.addEquipment(savedFurniture[x]);
            }
        } else {
            this.addEquipment();
        }
    };

    getSavedEquipment() {
        let savedFurniture = localStorage.getItem('work_tools');
        savedFurniture = JSON.parse(savedFurniture);
        if (savedFurniture) {

            return savedFurniture['office_equipment'];
        }
        return [];
    }

    toggleEquipmentInfo(e) {
        // console.log(e.target.value);
        if (e.value == '1') {
            this.createEquipment();
        } else {
            this.clearEquipment(<FormArray>this.workToolForm.get('office_equipment'));
        }
    }

    clearEquipment = (formArray: FormArray) => {
        if (formArray && formArray.length) {
            while (formArray.length !== 0) {
                formArray.removeAt(0);
            }
        }

    };

    /** Furniture */
    newFurniture(obj?): FormGroup {
        if (obj) {
            return this.formBuilder.group({
                cost_description: [obj ? obj.cost_description : '',[Validators.required]],
                 cost_value: [obj ? obj.cost_value: '',[Validators.required]]
            });
        } else {
            return this.formBuilder.group({
                cost_description: '',
                cost_value: ''
            });
        }

    }

    get furniture(): FormArray {
        return this.workToolForm.get('office_furniture') as FormArray;
    }

    addFurniture(obj?) {
        // ;
        if (obj) {
            this.furniture.push(this.newFurniture(obj));
        } else {
            this.furniture.push(this.newFurniture());
        }

    }

    removeFurniture(i: number) {
        this.furniture.removeAt(i);
    }

    createFurniture = () => {
        let savedFurniture = this.getSavedFurniture();
        if (savedFurniture.length) {
            for (let x in savedFurniture) {
                this.addFurniture(savedFurniture[x]);
            }
        } else {
            this.addFurniture();
        }
    };

    getSavedFurniture() {
        let savedFurniture = localStorage.getItem('work_tools');
        savedFurniture = JSON.parse(savedFurniture);
        if (savedFurniture) {

            return savedFurniture['office_furniture'];
        }
        return [];
    }

    toggleFurnitureInfo(e) {
        // console.log(e.target.value);
        if (e.value == '1') {
            this.createFurniture();
        } else {
            this.clearFurnitureArray(<FormArray>this.workToolForm.get('office_furniture'));
        }
    }

    clearFurnitureArray = (formArray: FormArray) => {
        if (formArray && formArray.length) {
            while (formArray.length !== 0) {
                formArray.removeAt(0);
            }
        }

    };

    onSubmit() {
      if (!this.aiTaxOnlineService.validate(this.workToolForm)) {
        return false;
      }
        // stop here if form is invalid
        console.log(this.workToolForm.value);
        if (this.workToolForm.valid) {
            // if (!this.registerForm.invalid) {
            this.submitted = true;
            this.loading = true;
            localStorage.setItem('work_tools', JSON.stringify(this.workToolForm.value));

            this.aiTaxOnlineService.saveWorkTools(this.workToolForm.value)
                .pipe(first())
                .subscribe(
                    data => {
                        this.submitted = true;
                        this.loading = false;
                        this.toastrService.success('Work Tools Information saved successfully.', 'Success!');
                        this.router.navigate(['aitax-online/work-tools-cellular']);
                    },
                    errorRes => {
                        this.toastrService.error(errorRes.error.message, 'Error!');
                        this.loading = false;
                        this.submitted = false;
                    });
        } else {
            this.loading = false;
            this.submitted = false;
            this.error = '';
            // alert('invalid');
            this.aiTaxOnlineService.touchAllFields(this.workToolForm);
        }
    }

    back() {
      localStorage.setItem('work_tools', JSON.stringify(this.workToolForm.value));
      this.router.navigate(['aitax-online/entertainment-expenses']);
    }

    openModal = (modelId): void => {
        let ngbModalOptions: NgbModalOptions = {
            backdrop: 'static',
            keyboard: false,
            size: 'md',
            windowClass: 'modal_extraDOc',
        };
        this.modalRef = this.modalService.open(modelId, ngbModalOptions);
    };
}
