import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpouseAdditionalInfoComponent } from './spouse-additional-info.component';

describe('SpouseAdditionalInfoComponent', () => {
  let component: SpouseAdditionalInfoComponent;
  let fixture: ComponentFixture<SpouseAdditionalInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpouseAdditionalInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpouseAdditionalInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
