import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-spouse-additional-info',
  templateUrl: './spouse-additional-info.component.html',
  styleUrls: ['./spouse-additional-info.component.css']
})
export class SpouseAdditionalInfoComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Spouse Additional Information';
  spouseAdditionalInfoForm: FormGroup;
  loading: boolean = false;
  submitted: boolean = false;
  error: string = '';
  success: string = '';
  personalInfo: any;
  spouseInfo: any;
  days = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  year = [];

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(4);
  }

  ngOnInit() {
    super.ngOnInit()
    this.personalInfo = JSON.parse(localStorage.getItem('personal_information'));
    this.spouseInfo = JSON.parse(localStorage.getItem('spouse_information'));
    console.log(this.personalInfo);

    this.spouseAdditionalInfoForm = this.formBuilder.group({
      us_tax_residency: [''],
      plan_to_change_immigration: [''],
      plan_to_depart: [''],
      estate_plan_in_place: ['', Validators.required],
      us_tax_residency_spouse:[''],
      day: [''],
      month: [''],
      year: [''],
    });
    this.toggleValidation(this.personalInfo.citizenship_status);
    var storedInfo = localStorage.getItem('spouse_additional_info');
    if (storedInfo) {
      this.spouseAdditionalInfoForm.patchValue(JSON.parse(storedInfo));
    }
    for (let i = new Date().getFullYear(); i >= 1930; i--) {
      this.year.push(i);
    }
  }

  toggleValidation(citizenship) {
    let usTaxResidancy = this.spouseAdditionalInfoForm.get('us_tax_residency');
    let planToChangeImmigration = this.spouseAdditionalInfoForm.get('plan_to_change_immigration');
    let planToDepart = this.spouseAdditionalInfoForm.get('plan_to_depart');
    console.log(citizenship == '0');
    if (citizenship == '0') {
      usTaxResidancy.setValidators(null);
      planToChangeImmigration.setValidators(null);
      planToDepart.setValidators(null);
    } else {
      usTaxResidancy.setValidators([Validators.required]);
      planToChangeImmigration.setValidators([Validators.required]);
      planToDepart.setValidators([Validators.required]);
    }
  }

  onSubmit() {

    this.aiTaxOnlineService.validate(this.spouseAdditionalInfoForm)
    // stop here if form is invalid
    if (this.spouseAdditionalInfoForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('spouse_additional_info', JSON.stringify(this.spouseAdditionalInfoForm.value));
      this.aiTaxOnlineService.saveSpouseAdditionalInfo(this.spouseAdditionalInfoForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Spouse Additional Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/cohan-rule']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.spouseAdditionalInfoForm);
    }
  }

  back() {
    localStorage.setItem('spouse_additional_info', JSON.stringify(this.spouseAdditionalInfoForm.value));
    if (this.personalInfo.marital_status == '1') {
      this.router.navigate(['aitax-online/spouse-information']);

    } else {
      this.router.navigate(['aitax-online/personal-information']);
    }
  }

  test(day) {
    let _day = this.spouseAdditionalInfoForm.get('day').value;
    let _month = this.spouseAdditionalInfoForm.get('month').value;
    let _year = this.spouseAdditionalInfoForm.get('year').value;


    if (_day !== '' && _month !== '' && _year !== '') {
      this.spouseAdditionalInfoForm.patchValue({us_tax_residency: '' + _year + '-' + _month + '-' + _day});
    }
  }

}
