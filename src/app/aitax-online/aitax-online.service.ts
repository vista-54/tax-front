import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Config} from '../config/config';
import {Observable} from 'rxjs';
import {FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class AITaxOnlineService {
  constructor(private http: HttpClient, private config: Config, private toastrService: ToastrService) {
  }

  sectionStart(data) {
    const url = this.config.getConfig('apiUrl') + 'amount-section';
    return this.http.post(url, data);
  }

  sectionFinish(data) {
    const url = this.config.getConfig('apiUrl') + 'amount-section-update';
    return this.http.post(url, data);
  }

  unlock() {
    const url = this.config.getConfig('apiUrl') + 'unlock';
    return this.http.post(url, {});
  }

  saveIntroVideo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'fbar';
    return this.http.post(url, formData);
  }

  saveRpi(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'rpi';
    return this.http.post(url, formData);
  }

  saveGA(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'ga';
    return this.http.post(url, formData);
  }

  savePersonalInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'personal';
    return this.http.post(url, formData);
  }

  savePropertyDonations(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'property-donations';
    return this.http.post(url, formData);
  }

  saveVolExp(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'vol-exp';
    return this.http.post(url, formData);
  }

  saveTuitionRelExp(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'tuition-rel-exp';
    return this.http.post(url, formData);
  }

  saveTransportation(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'add-trans-question';
    return this.http.post(url, formData);
  }

  saveTuitionExpenses(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'tuition-exp';
    return this.http.post(url, formData);
  }

  saveAddOptionExp(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'add-option-exp';
    return this.http.post(url, formData);
  }

  saveHomeExpense(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'home-expense';
    return this.http.post(url, formData);
  }

  saveSmallItems(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'small-items';
    return this.http.post(url, formData);
  }

  saveBusinessExp(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'business-exp';
    return this.http.post(url, formData);
  }

  saveVehicleExp(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'vehicle-exp';
    return this.http.post(url, formData);
  }

  saveVehicleMaintenance(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'vehicle-maintenance';
    return this.http.post(url, formData);
  }

  saveVehicleInsurance(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'vehicle-insurance';
    return this.http.post(url, formData);
  }

  saveEntertainmentExp(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'entertainment-exp';
    return this.http.post(url, formData);
  }

  saveWorkTools(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'employee-business-part1';
    return this.http.post(url, formData);
  }

  saveWorkToolsCellular(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'employee-business-part2';
    return this.http.post(url, formData);
  }

  saveSpouseInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'spouses';
    return this.http.post(url, formData);
  }

  saveSpouseAdditionalInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'user-additional-info';
    return this.http.post(url, formData);
  }

  saveCharitableDonations(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'charitable-donations';
    return this.http.post(url, formData);
  }

  saveBasicInfoForm(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'basic-info';
    return this.http.post(url, formData);
  }

  saveDependentInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'dependant';
    for (let item of formData['dependents']) {
      delete item['day'];
      delete item['month'];
      delete item['year'];
    }
    return this.http.post(url, formData);
  }

  saveEmergencyPersonalInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'emergency-personal';
    return this.http.post(url, formData);
  }

  saveBlueCollarExpensesInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'blue-collar-exp';
    return this.http.post(url, formData);
  }

  saveBlueCollarExpenses2Info(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'blue-collar-exp-two';
    return this.http.post(url, formData);
  }

  saveEmergencyPersonalExpensesInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'emergency-personal-expenses';
    return this.http.post(url, formData);
  }

  savePocketMedicalExpensesInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'pocket-medical-expenses';
    return this.http.post(url, formData);
  }

  savePocketMedicalExpenses2Info(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'pocket-medical-expenses-two';
    return this.http.post(url, formData);
  }

  saveWorkToolExamInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'work-tool-exam';
    return this.http.post(url, formData);
  }

  saveWorkToolsResearchInfo(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'work-tool-research';
    return this.http.post(url, formData);
  }

  saveDepositInformation(formData: object): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'deposit-information';
    return this.http.post(url, formData);
  }

  conclusion(): Observable<any> {
    const url = this.config.getConfig('apiUrl') + 'submit';
    return this.http.post(url, {body: ''});
  }

  touchAllFields(form) {
    // touch all fields to show the error
    Object.keys(form.controls).forEach(field => {
      const control = form.get(field);
      control.markAsTouched({onlySelf: true});
    });
  }


  validate(form: FormGroup) {
    if (!form.valid) {
      for (const i in form.controls) {
        let control = form.controls[i];
        if (control.hasOwnProperty('controls')) {
          let formsArray: FormGroup[];
          formsArray = control['controls'];
          for (const k in formsArray) {
            this.validate(formsArray[k])
          }
        }
        let errors = control.errors
        for (const j in errors) {
          let error = errors[j];
        return   this.showError(i, j, error);
        }

      }
    }
    else{
      return  true;
    }
  }

  showError(field, type, error) {
    let msg;
    console.log(field, type, error)
    switch (type) {
      case 'required':
        msg = "The " + field + ' field is required';
        break;
      case 'maxlength':
        msg = "The " + field + ' must be max ' + error.requiredLength + ' symbols of length';
        break;
      case 'email':
        msg = "The email field has a wrong format";
        break;
      default :
        msg = "Some validation issue";
    }
    this.toastrService.warning(msg, 'Warning!');
    console.log(msg);
    return false;
  }

}
