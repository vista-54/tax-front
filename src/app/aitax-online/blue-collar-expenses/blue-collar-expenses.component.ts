import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {NgbModalOptions, NgbModalRef, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-blue-collar-expenses',
  templateUrl: './blue-collar-expenses.component.html',
  styleUrls: ['./blue-collar-expenses.component.css']
})
export class BlueCollarExpensesComponent  extends ActivityTimeTracker implements OnInit {
  section_name = 'Blue Collar Expenses'
  blueCollarExpensesForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  modalRef: NgbModalRef;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private modalService: NgbModal,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(27);
  }

  ngOnInit() {
    super.ngOnInit()
    this.blueCollarExpensesForm = this.formBuilder.group({
      expenses_equipment_radio: ['', Validators.required],
      expenses_equipment: this.formBuilder.array([]),
    });

    const storedInfo = localStorage.getItem('blue_collar_expenses');
    if (storedInfo) {
      this.blueCollarExpensesForm.patchValue(JSON.parse(storedInfo));
    }

    if (this.blueCollarExpensesForm.get('expenses_equipment_radio').value === '1') {
      this.createEquipments();
    }

  }


  newPurchasingMachinery(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: '',
        cost_value: ''
      });
    }

  }

  get purchasingMachinery(): FormArray {
    return this.blueCollarExpensesForm.get('purchasing_machinery') as FormArray;
  }

  addPurchasingMachinery(obj?) {
    // ;
    if (obj) {
      this.purchasingMachinery.push(this.newPurchasingMachinery(obj));
    } else {
      this.purchasingMachinery.push(this.newPurchasingMachinery());
    }

  }

  removePurchasingMachinery(i: number) {
    this.purchasingMachinery.removeAt(i);
  }

  createPurchasingMachinery = () => {
    let savedUniforms = this.getSavedPurchasingMachinery();
    if (savedUniforms.length) {
      for (let x in savedUniforms) {
        this.addPurchasingMachinery(savedUniforms[x]);
      }
    } else {
      this.addPurchasingMachinery();
    }
  };

  getSavedPurchasingMachinery() {
    let savedPurchasingMachinery = localStorage.getItem('blue_collar_expenses');
    savedPurchasingMachinery = JSON.parse(savedPurchasingMachinery);
    if (savedPurchasingMachinery) {
      return savedPurchasingMachinery['purchasing_machinery'];
    }
    return [];
  }

  togglePurchasingMachineryInfo(e) {
    // console.log(e.target.value);
    if (e.target.value == '1') {
      this.createPurchasingMachinery();
    } else {
      this.clearPurchasingMachinery(<FormArray>this.blueCollarExpensesForm.get('purchasing_machinery'));
    }
  }

  clearPurchasingMachinery = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };


  /** Equipments */
  newEquipments(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: '',
        cost_value: ''
      });
    }

  }

  get equipments(): FormArray {
    return this.blueCollarExpensesForm.get('expenses_equipment') as FormArray;
  }

  addEquipments(obj?) {
    if (obj) {
      this.equipments.push(this.newEquipments(obj));
    } else {
      this.equipments.push(this.newEquipments());
    }

  }

  removeEquipments(i: number) {
    this.equipments.removeAt(i);
  }

  createEquipments = () => {
    let savedEquipments = this.getSavedEquipments();
    if (savedEquipments.length) {
      for (let x in savedEquipments) {
        this.addEquipments(savedEquipments[x]);
      }
    } else {
      this.addEquipments();
    }
  };

  getSavedEquipments() {
    let savedEquipments = localStorage.getItem('blue_collar_expenses');
    console.log(savedEquipments);
    savedEquipments = JSON.parse(savedEquipments);
    if (savedEquipments) {

      return savedEquipments['expenses_equipment'];
    }
    return [];
  }

  toggleExpensesEquipments(e) {
    // console.log(e.target.value);
    if (e.target.value == '1') {
      this.createEquipments();
    } else {
      this.clearUniformsArray(<FormArray>this.blueCollarExpensesForm.get('expenses_equipment'));
    }
  }

  clearUniformsArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  onSubmit() {
    // stop here if form is invalid
    console.log(this.blueCollarExpensesForm.value);
    if (this.blueCollarExpensesForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('blue_collar_expenses', JSON.stringify(this.blueCollarExpensesForm.value));

      this.aiTaxOnlineService.saveBlueCollarExpensesInfo(this.blueCollarExpensesForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Blue Collar Expenses Information saved successfully', 'Success!');
            this.router.navigate(['aitax-online/blue-collar-expenses-2']);
        },
        errorRes => {

            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.blueCollarExpensesForm);
    }
  }

  back() {
    localStorage.setItem('blue_collar_expenses', JSON.stringify(this.blueCollarExpensesForm.value));
    this.router.navigate(['aitax-online/work-tools-exam']);
  }

  openModal = (modelId): void => {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      size: 'md',
      windowClass: 'modal_extraDOc',
    };
    this.modalRef = this.modalService.open(modelId, ngbModalOptions);
  };

}
