import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlueCollarExpensesComponent } from './blue-collar-expenses.component';

describe('EmergencyPersonalExpensesComponent', () => {
  let component: BlueCollarExpensesComponent;
  let fixture: ComponentFixture<BlueCollarExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlueCollarExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlueCollarExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
