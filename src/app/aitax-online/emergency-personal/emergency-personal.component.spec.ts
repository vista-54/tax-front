import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkToolsResearchComponent } from './emergency-personal.component';

describe('WorkToolsResearchComponent', () => {
  let component: WorkToolsResearchComponent;
  let fixture: ComponentFixture<WorkToolsResearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkToolsResearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkToolsResearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
