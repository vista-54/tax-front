import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {AITaxOnlineService} from '../aitax-online.service';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
    selector: 'app-emergency-personal',
    templateUrl: './emergency-personal.component.html',
    styleUrls: ['./emergency-personal.component.css']
})
export class EmergencyPersonalComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Emergency Personnel'
    personalInfo: FormGroup;

    loading = false;
    submitted = false;
    error = '';
    success = '';

    constructor(private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private router: Router,
                public aiTaxOnlineService: AITaxOnlineService,
                private progress: ProgressService,
                private toastrService: ToastrService) {
      super(aiTaxOnlineService);
      progress.changeProgress(29);
    }

    ngOnInit() {
      super.ngOnInit()
        this.personalInfo = this.formBuilder.group({
            personal_status: ['', Validators.required]
        });
        const storedInfo = localStorage.getItem('emergency_personal');
        if (storedInfo) {
            const storedInformation = JSON.parse(storedInfo);
            this.personalInfo.patchValue(storedInformation);
        }
    }

    onSubmit() {

        // stop here if form is invalid
        console.log(this.personalInfo.value);
        if (this.personalInfo.valid) {
            // if (!this.registerForm.invalid) {
            this.submitted = true;
            this.loading = true;
            localStorage.setItem('emergency_personal', JSON.stringify(this.personalInfo.value));
            this.aiTaxOnlineService.saveEmergencyPersonalInfo(this.personalInfo.value)
                .pipe(first())
                .subscribe(
                    data => {
                        this.submitted = true;
                        this.loading = false;
                        this.toastrService.success('Emergency Personal Information saved successfully.', 'Success!');
                        if (this.personalInfo.get('personal_status').value === '1') {
                            this.router.navigate(['aitax-online/emergency-personal-expenses']);
                        } else {
                            this.router.navigate(['aitax-online/pocket-medical-expenses']);
                        }
                    },
                    errorRes => {
                        this.toastrService.error(errorRes.error.message, 'Error!');
                        this.loading = false;
                        this.submitted = false;
                    });
        } else {
            this.loading = false;
            this.submitted = false;
            this.error = '';
            this.aiTaxOnlineService.touchAllFields(this.personalInfo);
        }
    }

    back() {
      localStorage.setItem('emergency_personal', JSON.stringify(this.personalInfo.value));
      this.router.navigate(['aitax-online/blue-collar-expenses-2']);
    }

}
