import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CohanRuleComponent } from './cohan-rule.component';

describe('CohanRuleComponent', () => {
  let component: CohanRuleComponent;
  let fixture: ComponentFixture<CohanRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CohanRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CohanRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
