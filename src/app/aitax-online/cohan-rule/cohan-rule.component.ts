import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ProgressService} from '../../shared/services/progress.service';
import {ActivityTimeTracker} from "../ActivityTimeTracker";
import {AITaxOnlineService} from "../aitax-online.service";

@Component({
  selector: 'app-cohan-rule',
  templateUrl: './cohan-rule.component.html',
  styleUrls: ['./cohan-rule.component.css']
})
export class CohanRuleComponent extends ActivityTimeTracker {
  section_name = 'Cohan Rule';
  @ViewChild('video') video: ElementRef;
  public isVideoFinished: boolean = false;
  public storedInformation: Object;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private progress: ProgressService,
              public aiTaxOnlineService: AITaxOnlineService) {
    super(aiTaxOnlineService);
    progress.changeProgress(5);
    const storedInfo = localStorage.getItem('personal_information');
    if (storedInfo) {
      this.storedInformation = JSON.parse(storedInfo);
    }
    let videoFinished = localStorage.getItem('cohan_rule_video');
    if (videoFinished) {
      this.isVideoFinished = true;
    }

  }


  onEnd() {
    this.isVideoFinished = true;
    localStorage['cohan_rule_video'] = true;
  }

  onSubmit() {
    let country = '';
    debugger
    if (this.storedInformation.hasOwnProperty('address')) {
      country = this.storedInformation['address'];
    }
    if (country.indexOf('USA')!==-1) {
      this.router.navigate(['aitax-online/basic-information']);
    } else {
      this.router.navigate(['aitax-online/dependent-information']);
    }
  }

  back() {
    this.router.navigate(['aitax-online/spouse-additional-info']);
  }
}
