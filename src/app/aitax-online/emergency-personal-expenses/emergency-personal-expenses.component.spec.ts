import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyPersonalExpensesComponent } from './emergency-personal-expenses.component';

describe('EmergencyPersonalExpensesComponent', () => {
  let component: EmergencyPersonalExpensesComponent;
  let fixture: ComponentFixture<EmergencyPersonalExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergencyPersonalExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyPersonalExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
