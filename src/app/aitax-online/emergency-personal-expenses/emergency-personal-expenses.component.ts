import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first, mergeMap} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";
import {from} from "rxjs";

@Component({
  selector: 'app-emergency-personal-expenses',
  templateUrl: './emergency-personal-expenses.component.html',
  styleUrls: ['./emergency-personal-expenses.component.css']
})
export class EmergencyPersonalExpensesComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Emergency Personnel'
  emergencyPersonalExpensesForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService)
    progress.changeProgress(26);
  }

  ngOnInit() {
    super.ngOnInit()
    this.emergencyPersonalExpensesForm = this.formBuilder.group({
      personal_status: ['', Validators.required],
      weapons_radio: ['', Validators.required],
      weapons_arr: this.formBuilder.array([]),
      emergency_cost: this.formBuilder.array([]),
      training_cost: this.formBuilder.array([])
    });

    const storedInfo = localStorage.getItem('emergency_personal_expenses');
    if (storedInfo) {
      this.emergencyPersonalExpensesForm.patchValue(JSON.parse(storedInfo));
    }
    if (this.emergencyPersonalExpensesForm.get('weapons_radio').value === '1') {
      this.createWeapons();
    }
    this.createEmergencyCost();
    this.createTrainingCost();


  }


  newWeapons(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get weapons(): FormArray {
    return this.emergencyPersonalExpensesForm.get('weapons_arr') as FormArray;
  }

  addWeapons(obj?) {
    if (obj) {
      this.weapons.push(this.newWeapons(obj));
    } else {
      this.weapons.push(this.newWeapons());
    }

  }

  removeWeapons(i: number) {
    this.weapons.removeAt(i);
  }

  createWeapons = () => {
    let savedWeapons = this.getSavedWeapons();
    if (savedWeapons.length) {
      for (let x in savedWeapons) {
        this.addWeapons(savedWeapons[x]);
      }
    } else {
      this.addWeapons();
    }
  };

  getSavedWeapons() {
    let savedWeapons = localStorage.getItem('emergency_personal_expenses');
    console.log(savedWeapons);
    savedWeapons = JSON.parse(savedWeapons);
    if (savedWeapons) {
      return savedWeapons['weapons_arr'];
    }
    return [];
  }

  toggleWeapons(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createWeapons();
    } else {
      this.clearArray(<FormArray>this.emergencyPersonalExpensesForm.get('weapons_arr'));
    }
  }

  //
  //
  //

  newEmergencyCost(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get emergencyCost(): FormArray {
    return this.emergencyPersonalExpensesForm.get('emergency_cost') as FormArray;
  }

  addEmergencyCost(obj?) {
    if (obj) {
      this.emergencyCost.push(this.newEmergencyCost(obj));
    } else {
      this.emergencyCost.push(this.newEmergencyCost());
    }

  }

  removeEmergencyCost(i: number) {
    this.emergencyCost.removeAt(i);
  }

  createEmergencyCost = () => {
    let savedEmergencyCost = this.getSavedEmergencyCost();
    if (savedEmergencyCost.length) {
      for (let x in savedEmergencyCost) {
        this.addEmergencyCost(savedEmergencyCost[x]);
      }
    } else {
      this.addEmergencyCost();
    }
  };

  getSavedEmergencyCost() {
    let savedEmergencyCost = localStorage.getItem('emergency_personal_expenses');
    console.log(savedEmergencyCost);
    savedEmergencyCost = JSON.parse(savedEmergencyCost);
    if (savedEmergencyCost) {
      return savedEmergencyCost['emergency_cost'];
    }
    return [];
  }


  ///

  newTrainingCost(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: ['', Validators.required],
        cost_value: ['', Validators.required]
      });
    }

  }

  get trainingCost(): FormArray {
    return this.emergencyPersonalExpensesForm.get('training_cost') as FormArray;
  }

  addTrainingCost(obj?) {
    if (obj) {
      this.trainingCost.push(this.newTrainingCost(obj));
    } else {
      this.trainingCost.push(this.newTrainingCost());
    }

  }

  removeTrainingCost(i: number) {
    this.trainingCost.removeAt(i);
  }

  createTrainingCost = () => {
    let savedTrainingCost = this.getSavedTrainingCost();
    if (savedTrainingCost.length) {
      for (let x in savedTrainingCost) {
        this.addTrainingCost(savedTrainingCost[x]);
      }
    } else {
      this.addTrainingCost();
    }
  };

  getSavedTrainingCost() {
    let savedTrainingCost = localStorage.getItem('emergency_personal_expenses');
    console.log(savedTrainingCost);
    savedTrainingCost = JSON.parse(savedTrainingCost);
    if (savedTrainingCost) {
      return savedTrainingCost['training_cost'];
    }
    return [];
  }


  onSubmit() {
    if (!this.aiTaxOnlineService.validate(this.emergencyPersonalExpensesForm)) {
      return false;
    }
    // stop here if form is invalid
    console.log(this.emergencyPersonalExpensesForm.value);
    if (this.emergencyPersonalExpensesForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      const emergencyPersonal = {
        request: 'em',
        personal_status: this.emergencyPersonalExpensesForm.get('personal_status').value
      }
      const emergencyPersonalExpensesForm = this.emergencyPersonalExpensesForm.value;

      from([emergencyPersonal, emergencyPersonalExpensesForm])
        .pipe(
          mergeMap(param => this.sendToServer(param)),
          first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            localStorage.setItem('emergency_personal', JSON.stringify(emergencyPersonal));
            localStorage.setItem('emergency_personal_expenses', JSON.stringify(this.emergencyPersonalExpensesForm.value));

            this.toastrService.success('Emergency Personal Expense Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/pocket-medical-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.toastrService.warning('Please fill in all fields', 'Warning!');
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.emergencyPersonalExpensesForm);
    }
  }

  back() {
    localStorage.setItem('emergency_personal_expenses', JSON.stringify(this.emergencyPersonalExpensesForm.value));
    this.router.navigate(['aitax-online/blue-collar-expenses-2']);
  }

  clearArray = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  sendToServer(obj) {
    switch (obj.request) {
      case 'em':
        return this.aiTaxOnlineService.saveEmergencyPersonalInfo(obj)
      default:
        return this.aiTaxOnlineService.saveEmergencyPersonalExpensesInfo(obj);
    }
  }

}
