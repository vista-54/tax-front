import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallItemsComponent } from './small-items.component';

describe('SmallItemsComponent', () => {
  let component: SmallItemsComponent;
  let fixture: ComponentFixture<SmallItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmallItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
