import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-small-items',
  templateUrl: './small-items.component.html',
  styleUrls: ['./small-items.component.css']
})
export class SmallItemsComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Small But Important Items';
  smallItemsForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(15);
  }

  ngOnInit() {
    super.ngOnInit();
    this.smallItemsForm = this.formBuilder.group({
      sii_traditional_IRA_contributions_radio: ['', Validators.required],
      sii_traditional_IRA_contributions: [''],
      sii_roth_IRA_contributions: [''],
      are_you_teacher_radio: ['', [Validators.required]],
      sii_educator_expenses_radio: [''],
      sii_educator_expenses: [''],
      sii_self_employed_radio: [''],
      sii_total_mortgage_payments: [''],
      sii_total_HOA: [''],
      sii_total_utilities: ['']
    });
    const storedInfo = localStorage.getItem('small_items');
    if (storedInfo) {
      this.smallItemsForm.patchValue(JSON.parse(storedInfo));
    }
  }

  onSubmit() {

    if (!this.aiTaxOnlineService.validate(this.smallItemsForm)) {
      return false;
    }
    // stop here if form is invalid
    console.log(this.smallItemsForm.value);
    if (this.smallItemsForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('small_items', JSON.stringify(this.smallItemsForm.value));

      this.aiTaxOnlineService.saveSmallItems(this.smallItemsForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Small Important Items Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/business-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.smallItemsForm);
    }
  }

  back() {
    localStorage.setItem('small_items', JSON.stringify(this.smallItemsForm.value));
    this.router.navigate(['aitax-online/home-expenses']);
  }
}
