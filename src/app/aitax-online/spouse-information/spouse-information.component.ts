import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AITaxOnlineService} from '../aitax-online.service';
import {ActivatedRoute, Router} from '@angular/router';
import {COUNTRIES} from '../../shared/constants/countries';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {phoneCodes} from '../../shared/constants/phone-codes';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-spouse-information',
  templateUrl: './spouse-information.component.html',
  styleUrls: ['./spouse-information.component.css']
})
export class SpouseInformationComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Spouse Information';

  spouseInfoForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  public otherCitizen: string[] = [];
  days = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'];
  month = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  year = [];
  phoneCods = phoneCodes;


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService);
    progress.changeProgress(3);


  }

  ngOnInit() {
    super.ngOnInit();
    this.spouseInfoForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      middle_name: [''],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required]],
      // country_code: ['', Validators.required],
      visa_type: ['', []],
      citizenship_status: ['', Validators.required],
      social_security: [''],
      citizenship_other: [''],
      job_title: ['', Validators.required],
      date_of_birth: ['', Validators.required],
      day: ['',Validators.required],
      month: ['',Validators.required],
      year: ['',Validators.required],
    });


    var storedInfo = localStorage.getItem('spouse_information');
    if (storedInfo) {
      this.spouseInfoForm.patchValue(JSON.parse(storedInfo));
    }
    for (let i = new Date().getFullYear(); i >= 1930; i--) {
      this.year.push(i);
    }
    this.otherCitizen = COUNTRIES;

  }

  onSubmit() {

    // stop here if form is invalid
    this.aiTaxOnlineService.validate(this.spouseInfoForm)
    if (this.spouseInfoForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('spouse_information', JSON.stringify(this.spouseInfoForm.value));

      this.aiTaxOnlineService.saveSpouseInfo(this.spouseInfoForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Spouse Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/spouse-additional-info']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.spouseInfoForm);
    }
  }

  back() {
    localStorage.setItem('spouse_information', JSON.stringify(this.spouseInfoForm.value));
    this.router.navigate(['aitax-online/personal-information']);
  }

  test() {
    let _day = this.spouseInfoForm.get('day').value;
    let _month = this.spouseInfoForm.get('month').value;
    let _year = this.spouseInfoForm.get('year').value;


    if (_day !== '' && _month !== '' && _year !== '') {
      this.spouseInfoForm.patchValue({date_of_birth: '' + _year + '-' + _month + '-' + _day});
    }
  }

}
