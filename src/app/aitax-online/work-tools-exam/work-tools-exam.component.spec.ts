import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkToolsExamComponent } from './work-tools-exam.component';

describe('WorkToolsExamComponent', () => {
  let component: WorkToolsExamComponent;
  let fixture: ComponentFixture<WorkToolsExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkToolsExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkToolsExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
