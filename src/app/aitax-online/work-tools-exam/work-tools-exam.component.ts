import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first, mergeMap} from 'rxjs/operators';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";
import {from} from "rxjs";


@Component({
  selector: 'app-work-tools-exam',
  templateUrl: './work-tools-exam.component.html',
  styleUrls: ['./work-tools-exam.component.css']
})
export class WorkToolsExamComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Work Tools & Misc';
  workToolExamForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  isMarried: boolean = false;
  modalRef: NgbModalRef;


  constructor(private formBuilder: FormBuilder,
              private router: Router,
              public aiTaxOnlineService: AITaxOnlineService,
              private modalService: NgbModal,
              private progress: ProgressService,
              private toastrService: ToastrService) {
    super(aiTaxOnlineService)
    progress.changeProgress(24);

  }

  ngOnInit() {
    super.ngOnInit()
    this.workToolExamForm = this.formBuilder.group({
      professional_fees_radio: ['', Validators.required],
      professional_fees: this.formBuilder.array([]),
      professional_dev_radio: ['', Validators.required],
      professional_dev: [''],
      professional_dev_spouse: [''],
      membership_fees_radio: ['', Validators.required],
      membership_fees: this.formBuilder.array([]),
      research_radio: ['', Validators.required],
      research: this.formBuilder.array([]),
    });

    const personalInfo = localStorage.getItem('personal_information');
    if (personalInfo) {
      this.isMarried = JSON.parse(personalInfo).marital_status === '1';
    }
    let storedInfo = localStorage.getItem('work_tools_exam');
    if (storedInfo) {
      this.workToolExamForm.patchValue(JSON.parse(storedInfo));
    }

    let research = localStorage.getItem('work_tools_research');
    if (research) {
      this.workToolExamForm.patchValue(JSON.parse(research));
    }


    if (this.workToolExamForm.get('professional_fees_radio').value == '1') {
      this.createProfesionalFees();
    }
    if (this.workToolExamForm.get('membership_fees_radio').value == '1') {
      this.createMembershipFees();
    }
    if (this.workToolExamForm.get('research_radio').value == '1') {
      this.createResearch();
    }


  }


  /**/

  newProfesionalFees(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: '',
        cost_value: ''
      });
    }

  }

  get professionalFees(): FormArray {
    return this.workToolExamForm.get('professional_fees') as FormArray;
  }

  addProfesionalFees(obj?) {
    // ;
    if (obj) {
      this.professionalFees.push(this.newProfesionalFees(obj));
    } else {
      this.professionalFees.push(this.newProfesionalFees());
    }

  }

  removeProfesionalFees(i: number) {
    this.professionalFees.removeAt(i);
  }

  createProfesionalFees = () => {
    let savedProfesionalFees = this.getSavedProfesionalFees();
    if (savedProfesionalFees.length) {
      for (let x in savedProfesionalFees) {
        this.addProfesionalFees(savedProfesionalFees[x]);
      }
    } else {
      this.addProfesionalFees();
    }
  };

  getSavedProfesionalFees() {
    let savedProfesionalFees = localStorage.getItem('work_tools_exam');
    savedProfesionalFees = JSON.parse(savedProfesionalFees);
    if (savedProfesionalFees) {
      return savedProfesionalFees['professional_fees'];
    }
    return [];
  }

  toggleProfesionalFeesInfo(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createProfesionalFees();
    } else {
      this.clearProfesionalFees(<FormArray>this.workToolExamForm.get('professional_fees'));
    }
  }

  clearProfesionalFees = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };

  //

  newMembershipFees(obj?): FormGroup {
    if (obj) {
      return this.formBuilder.group({
        cost_description: [obj ? obj.cost_description : '',[Validators.required]],
         cost_value: [obj ? obj.cost_value: '',[Validators.required]]
      });
    } else {
      return this.formBuilder.group({
        cost_description: '',
        cost_value: ''
      });
    }

  }

  get membershipFees(): FormArray {
    return this.workToolExamForm.get('membership_fees') as FormArray;
  }

  get research(): FormArray {
    return this.workToolExamForm.get('research') as FormArray;
  }

  addMembershipFees(obj?) {
    // ;
    if (obj) {
      this.membershipFees.push(this.newMembershipFees(obj));
    } else {
      this.membershipFees.push(this.newMembershipFees());
    }

  }

  addResearch(obj?) {
    // ;
    if (obj) {
      this.research.push(this.newMembershipFees(obj));
    } else {
      this.research.push(this.newMembershipFees());
    }

  }

  removeMembershipFees(i: number) {
    this.membershipFees.removeAt(i);
  }

  createMembershipFees = () => {
    let savedMembershipFees = this.getSavedMembershipFees();
    if (savedMembershipFees.length) {
      for (let x in savedMembershipFees) {
        this.addMembershipFees(savedMembershipFees[x]);
      }
    } else {
      this.addMembershipFees();
    }
  };

  createResearch = () => {
    let savedMembershipFees = this.getSavedResearches();
    if (savedMembershipFees.length) {
      for (let x in savedMembershipFees) {
        this.addResearch(savedMembershipFees[x]);
      }
    } else {
      this.addResearch();
    }
  };

  getSavedMembershipFees() {
    let savedMembershipFees = localStorage.getItem('work_tools_exam');
    savedMembershipFees = JSON.parse(savedMembershipFees);
    if (savedMembershipFees) {
      return savedMembershipFees['membership_fees'];
    }
    return [];
  }

  getSavedResearches() {
    let savedMembershipFees = localStorage.getItem('work_tools_research');
    savedMembershipFees = JSON.parse(savedMembershipFees);
    if (savedMembershipFees) {
      return savedMembershipFees['research'];
    }
    return [];
  }

  toggleMembershipFeesInfo(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createMembershipFees();
    } else {
      this.clearMembershipFees(<FormArray>this.workToolExamForm.get('membership_fees'));
    }
  }

  toggleResearchInfo(e) {
    // console.log(e.target.value);
    if (e.value == '1') {
      this.createResearch();
    } else {
      this.clearMembershipFees(<FormArray>this.workToolExamForm.get('research'));
    }
  }

  clearMembershipFees = (formArray: FormArray) => {
    if (formArray && formArray.length) {
      while (formArray.length !== 0) {
        formArray.removeAt(0);
      }
    }

  };


  onSubmit() {
    if (!this.aiTaxOnlineService.validate(this.workToolExamForm)) {
      return false;
    }
    // stop here if form is invalid
    console.log(this.workToolExamForm.valid);
    if (this.workToolExamForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      const workToolObj = this.workToolExamForm.value;
      let internet_pay = localStorage.getItem('internet_pay');

      let workResearchObj = {
        request: 'research',
        research_radio: this.workToolExamForm.get('research_radio').value,
        research: this.workToolExamForm.get('research').value,
        internet_pay: '0'
      };
      if (internet_pay) {
        workResearchObj.internet_pay = internet_pay;
      }

      from([workToolObj, workResearchObj])
        .pipe(
          mergeMap(param => this.sendToServer(param)),
          first())
        .subscribe(
          data => {
            localStorage.setItem('work_tools_exam', JSON.stringify(workToolObj));
            localStorage.setItem('work_tools_research', JSON.stringify(workResearchObj));
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Work Tools Exam Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/blue-collar-expenses-2']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.workToolExamForm);
    }
  }

  back() {
    localStorage.setItem('work_tools_exam', JSON.stringify(this.workToolExamForm.value));
    this.router.navigate(['aitax-online/work-tools-cellular']);
  }

  openModal = (modelId): void => {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      size: 'md',
      windowClass: 'modal_extraDOc',
    };
    this.modalRef = this.modalService.open(modelId, ngbModalOptions);
  };


  sendToServer(obj) {
    switch (obj.request) {
      case 'research':
        return this.aiTaxOnlineService.saveWorkToolsResearchInfo(obj);
      default:
        return this.aiTaxOnlineService.saveWorkToolExamInfo(obj);
    }
  }
}
