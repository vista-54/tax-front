import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyDonationsComponent } from './property-donations.component';

describe('PropertyDonationsComponent', () => {
  let component: PropertyDonationsComponent;
  let fixture: ComponentFixture<PropertyDonationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyDonationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyDonationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
