import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first} from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
    selector: 'app-property-donations',
    templateUrl: './property-donations.component.html',
    styleUrls: ['./property-donations.component.css']
})
export class PropertyDonationsComponent extends ActivityTimeTracker  implements  OnInit {
    section_name = 'Property Donations';
    propertyDonationsForm: FormGroup;
    loading = false;
    submitted = false;
    error = '';
    success = '';

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                public aiTaxOnlineService: AITaxOnlineService,
                private progress: ProgressService,
                private toastrService: ToastrService) {
      super(aiTaxOnlineService)
      progress.changeProgress(12);
    }

    ngOnInit() {
      super.ngOnInit();
        this.propertyDonationsForm = this.formBuilder.group({
            pd_checkbox: ['', Validators.required],
            donations: this.formBuilder.array([]),
        });

        const storedInfo = localStorage.getItem('property_donations');
        if (storedInfo) {
            this.propertyDonationsForm.patchValue(JSON.parse(storedInfo));
        }

        if (this.propertyDonationsForm.get('pd_checkbox').value == '1') {
            // ;
            this.createDonations();
        }
    }

    newDonation(obj?): FormGroup {
        if (obj) {
            return this.formBuilder.group({
                cost_description: [obj ? obj.cost_description : '',[Validators.required]],
                 cost_value: [obj ? obj.cost_value: '',[Validators.required]]
            });
        } else {
            return this.formBuilder.group({
              cost_description: ['', Validators.required],
              cost_value: ['', Validators.required]
            });
        }

    }

    get donations(): FormArray {
        return this.propertyDonationsForm.get('donations') as FormArray;
    }

    addDonation(obj?) {
        // ;
        if (obj) {
            this.donations.push(this.newDonation(obj));
        } else {
            this.donations.push(this.newDonation());
        }

    }

    removeDonation(i: number) {
        this.donations.removeAt(i);
    }

    createDonations = () => {
        let donation = this.getSavedDonations();
      console.log(donation);
      if (donation !== undefined && donation.length) {
            for (let x in donation) {
                this.addDonation(donation[x]);
            }
        } else {
            this.addDonation();
        }
    };

    getSavedDonations() {
        let donationsInformation = localStorage.getItem('property_donations');
      console.log(donationsInformation);
      donationsInformation = JSON.parse(donationsInformation);
        if (donationsInformation) {

            return donationsInformation['donations'];
        }
        return [];
    }

    toggleDependentsInfo(e) {
        console.log(e.target.value);
        if (e.target.value === '1') {
            this.createDonations();
        } else {
            this.clearFormArray(<FormArray>this.propertyDonationsForm.get('donations'));
        }
    }

    clearFormArray = (formArray: FormArray) => {
        if (formArray && formArray.length) {
            while (formArray.length !== 0) {
                formArray.removeAt(0);
            }
        }

    };

  onSubmit() {


    // stop here if form is invalid
    console.log(this.propertyDonationsForm.value);
    if (this.propertyDonationsForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      if (this.propertyDonationsForm.get('pd_checkbox').value === '0') {
        this.propertyDonationsForm.removeControl('donations');
      }
      localStorage.setItem('property_donations', JSON.stringify(this.propertyDonationsForm.value));
      this.aiTaxOnlineService.savePropertyDonations(this.propertyDonationsForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Property Donations Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/volunter-expenses']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.propertyDonationsForm);
    }
  }

  back() {
    localStorage.setItem('property_donations', JSON.stringify(this.propertyDonationsForm.value));
    this.router.navigate(['aitax-online/charitable-donations']);
  }

}
