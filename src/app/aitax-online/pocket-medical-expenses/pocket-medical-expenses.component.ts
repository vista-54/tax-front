import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {first, mergeMap} from 'rxjs/operators';

import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";
import {from} from "rxjs";


@Component({
  selector: 'app-pocket-medical-expenses',
  templateUrl: './pocket-medical-expenses.component.html',
  styleUrls: ['./pocket-medical-expenses.component.css']
})
export class PocketMedicalExpensesComponent extends ActivityTimeTracker implements OnInit {

  section_name = 'Out of Pocket Medical Expenses';
  pocketMedicalExpensesForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  modalRef: NgbModalRef;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public aiTaxOnlineService: AITaxOnlineService,
    private progress: ProgressService,
    private modalService: NgbModal,
    private toastrService: ToastrService,
  ) {
    super(aiTaxOnlineService)
    progress.changeProgress(27);
  }

  ngOnInit() {
    super.ngOnInit()
    this.pocketMedicalExpensesForm = this.formBuilder.group({
      insurance: ['', Validators.required],
      insurance_non: ['', Validators.required],
      out_of_pocket_radio: ['', Validators.required],
      out_of_pocket: [''],
      out_of_pocket_non: [''],

      out_of_pocket_medications_radio: ['', Validators.required],
      out_of_pocket_medications: [''],
      out_of_pocket_dental_radio: ['', Validators.required],
      out_of_pocket_dental: [''],
      out_of_pocket_other_radio: ['', Validators.required],
      out_of_pocket_other: ['']
    });

    let storedInfo = localStorage.getItem('pocket_medical_expenses');
    console.log(JSON.parse(storedInfo));
    if (storedInfo) {
      this.pocketMedicalExpensesForm.patchValue(JSON.parse(storedInfo));
    }


  }

  onSubmit() {
    if (!this.aiTaxOnlineService.validate(this.pocketMedicalExpensesForm)) {
      return false;
    }
    console.log(this.pocketMedicalExpensesForm.value);
    if (this.pocketMedicalExpensesForm.valid) {
      this.submitted = true;
      this.loading = true;
//
      const pocketOnePartObj = this.pocketMedicalExpensesForm.value;
      const pocketTwoPartObj = {
        request: 'two',
        out_of_pocket_medications_radio: this.pocketMedicalExpensesForm.get('out_of_pocket_medications_radio').value,
        out_of_pocket_medications: this.pocketMedicalExpensesForm.get('out_of_pocket_medications').value,
        out_of_pocket_dental_radio: this.pocketMedicalExpensesForm.get('out_of_pocket_dental_radio').value,
        out_of_pocket_dental: this.pocketMedicalExpensesForm.get('out_of_pocket_dental').value,
        out_of_pocket_other_radio: this.pocketMedicalExpensesForm.get('out_of_pocket_other_radio').value,
        out_of_pocket_other: this.pocketMedicalExpensesForm.get('out_of_pocket_other').value
      }

      from([pocketOnePartObj, pocketTwoPartObj])
        .pipe(
          mergeMap(param => this.sendToServer(param)),
          first())
        .subscribe(
          data => {
            localStorage.setItem('pocket_medical_expenses', JSON.stringify(this.pocketMedicalExpensesForm.value));
            this.submitted = true;
            this.loading = false;
            this.toastrService.success('Pocket Medical Expense Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/deposit-information']);
          },
          errorRes => {
            // const message = this.aiTaxErrorHanlder.handleError(error.status, error.error);
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      this.aiTaxOnlineService.touchAllFields(this.pocketMedicalExpensesForm);
    }
  }

  back() {
    localStorage.setItem('pocket_medical_expenses', JSON.stringify(this.pocketMedicalExpensesForm.value));
    this.router.navigate(['aitax-online/emergency-personal-expenses']);
  }


  openModal = (modelId): void => {
    let ngbModalOptions: NgbModalOptions = {
      backdrop: 'static',
      keyboard: false,
      size: 'md',
      windowClass: 'modal_extraDOc',
    };
    this.modalRef = this.modalService.open(modelId, ngbModalOptions);
  };

  sendToServer(obj) {
    switch (obj.request) {
      case 'two':
        return this.aiTaxOnlineService.savePocketMedicalExpenses2Info(obj)
      default:
        return this.aiTaxOnlineService.savePocketMedicalExpensesInfo(obj)

    }
  }
}
