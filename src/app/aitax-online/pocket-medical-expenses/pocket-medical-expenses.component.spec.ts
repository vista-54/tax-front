import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PocketMedicalExpensesComponent } from './pocket-medical-expenses.component';

describe('DepositInformationComponent', () => {
  let component: PocketMedicalExpensesComponent;
  let fixture: ComponentFixture<PocketMedicalExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PocketMedicalExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PocketMedicalExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
