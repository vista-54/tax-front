import {Component, OnInit} from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AITaxOnlineService} from '../aitax-online.service';
import {ProgressService} from '../../shared/services/progress.service';
import {ToastrService} from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-rental-property-information',
  templateUrl: './rental-property-information.component.html',
  styleUrls: ['./rental-property-information.component.css']
})
export class RentalPropertyInformationComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'Rental Property Information';
  rentalPropertyForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public aiTaxOnlineService: AITaxOnlineService,
    private progress: ProgressService,
    private toastrService: ToastrService
  ) {
    super(aiTaxOnlineService);
    progress.changeProgress(9);
  }

  ngOnInit() {
    super.ngOnInit();
    this.rentalPropertyForm = this.formBuilder.group({
      rental_home: ['', Validators.required]
    });

    var storedInfo = localStorage.getItem('rental_property');
    if (storedInfo) {
      this.rentalPropertyForm.patchValue(JSON.parse(storedInfo));
    }
  }

  onSubmit() {


    if (!this.aiTaxOnlineService.validate(this.rentalPropertyForm)) {
      return false;
    }
    // stop here if form is invalid
    if (this.rentalPropertyForm.valid) {
      // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;

      this.aiTaxOnlineService.saveRpi(this.rentalPropertyForm.value)
        .pipe(first())
        .subscribe(
          data => {
            this.submitted = true;
            localStorage.setItem('rental_property', JSON.stringify(this.rentalPropertyForm.value));
            this.loading = false;
            this.toastrService.success('Rental Property Information saved successfully.', 'Success!');
            this.router.navigate(['aitax-online/side-income-information']);
          },
          errorRes => {
            this.toastrService.error(errorRes.error.message, 'Error!');
            this.loading = false;
            this.submitted = false;
          });
    } else {
      this.loading = false;
      this.submitted = false;
      this.error = '';
      // alert('invalid');
      this.aiTaxOnlineService.touchAllFields(this.rentalPropertyForm);
    }
  }


  back() {
    localStorage.setItem('rental_property', JSON.stringify(this.rentalPropertyForm.value));
    this.router.navigate(['aitax-online/fbar-information']);
  }

}
