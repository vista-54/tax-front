import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RentalPropertyInformationComponent } from './rental-property-information.component';

describe('RentalPropertyInformationComponent', () => {
  let component: RentalPropertyInformationComponent;
  let fixture: ComponentFixture<RentalPropertyInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RentalPropertyInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RentalPropertyInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
