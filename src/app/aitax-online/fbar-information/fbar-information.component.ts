import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AITaxOnlineService } from '../aitax-online.service';
import { first } from 'rxjs/operators';
import {ProgressService} from '../../shared/services/progress.service';
import { ToastrService } from 'ngx-toastr';
import {ActivityTimeTracker} from "../ActivityTimeTracker";

@Component({
  selector: 'app-fbar-information',
  templateUrl: './fbar-information.component.html',
  styleUrls: ['./fbar-information.component.css']
})
export class FbarInformationComponent extends ActivityTimeTracker implements OnInit {
  section_name = 'FBAR';
  fbarForm: FormGroup;
  loading = false;
  submitted = false;
  error = '';
  success = '';
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    public aiTaxOnlineService: AITaxOnlineService,
    private progress: ProgressService,
    private toastrService: ToastrService
  ) {
    super(aiTaxOnlineService);
    progress.changeProgress(8);
  }

  ngOnInit() {
    super.ngOnInit()
    this.fbarForm = this.formBuilder.group({
      fbr_outside_money: ['', Validators.required]
    });

    var storedInfo = localStorage.getItem('fbar_information');
    if(storedInfo){
      this.fbarForm.patchValue(JSON.parse(storedInfo))
    }
  }

  onSubmit() {


    // stop here if form is invalid
    if (!this.aiTaxOnlineService.validate(this.fbarForm)) {
      return false;
    }
    if (this.fbarForm.valid) {
    // if (!this.registerForm.invalid) {
      this.submitted = true;
      this.loading = true;
      localStorage.setItem('fbar_information', JSON.stringify(this.fbarForm.value))

      this.aiTaxOnlineService.saveIntroVideo(this.fbarForm.value)
        .pipe(first())
        .subscribe(
            data => {
              this.submitted = true;
              this.loading = false;
              this.toastrService.success('FBAR Information saved successfully.', 'Success!');
              this.router.navigate(['aitax-online/rental-property-information'])
            },
            errorRes => {
              this.toastrService.error(errorRes.error.message, 'Error!');
                this.loading = false;
                this.submitted = false;
            });
      } else {
        this.loading = false;
        this.submitted = false;
        this.error = ''
        // alert('invalid');
        this.aiTaxOnlineService.touchAllFields(this.fbarForm)
      }
  }

  back(){
    localStorage.setItem('fbar_information', JSON.stringify(this.fbarForm.value))

    this.router.navigate(['aitax-online/dependent-information']);
  }
}
