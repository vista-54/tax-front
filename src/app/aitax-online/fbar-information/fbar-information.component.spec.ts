import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FbarInformationComponent } from './fbar-information.component';

describe('FbarInformationComponent', () => {
  let component: FbarInformationComponent;
  let fixture: ComponentFixture<FbarInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FbarInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FbarInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
