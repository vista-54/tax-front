import {Component, OnInit} from '@angular/core';
import {navItemsAdmin, navItemsUser} from '../../_nav';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';

declare const document;

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {

  role: any;
  public navItems: any;

  constructor(
    private authenticationService: AuthService,
    private router: Router
  ) {
    let body = document.getElementsByTagName('body');
    console.log(body[0].classList.remove('sidebar-lg-show'));
    // let nav = document.getElementsByTagName('app-sidebar');
    //
    // // console.log(main[0].style.marginLeft = 0);
    setTimeout(() => {
      body[0].classList.remove('sidebar-lg-show');
    }, 400);
    // main.style.marginLeft = '0px !important';
  }

  ngOnInit() {

    this.role = JSON.parse(localStorage.getItem('role'));
    if (this.role == 'ADMIN') {
      this.navItems = navItemsAdmin;
    } else {
      this.navItems = navItemsUser;
      // this.navItems = navItemsAdmin;
    }
  }

  public sidebarMinimized = false;

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
